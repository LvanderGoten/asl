__author__ = 'lennart'
from optparse import OptionParser
import json
import sys


def parse_set_callback(option, opt, value, parser):
    if '{' in value:
        setattr(parser.values, option.dest, [float(item) for item in value[1:-1].split(',')])
    else:
        setattr(parser.values, option.dest, float(value))


def main():
    print(sys.argv)
    # Parser for stdin
    parser = OptionParser()

    parser.add_option('--number_of_users',
                      help='Number of users',
                      metavar='NUM',
                      type='int')

    parser.add_option('--thinking_time',
                      help='the time the clients think',
                      metavar='MS',
                      default=0,
                      type='float')

    parser.add_option('--number_of_devices',
                      help='Number of devices (=M)',
                      metavar='NUM',
                      type='int')

    parser.add_option('--service_times_per_visit',
                      help='Service time per visit to the i-th device',
                      type='string',
                      action='callback',
                      callback=parse_set_callback,
                      metavar='{S_1,...,S_M}')

    parser.add_option('--number_of_visits',
                      help='Number of visits to the i-th device',
                      type='string',
                      action='callback',
                      callback=parse_set_callback,
                      default='',
                      metavar='{V_1,...,V_M}')

    parser.add_option('--replicate',
                      action='store_true',
                      default=False)

    parser.add_option('--is_delay_center',
                      default=False,
                      action='store_true')

    (options, args) = parser.parse_args()

    if options.replicate:
        S = [options.service_times_per_visit for _ in range(options.number_of_devices)]
        V = [options.number_of_visits for _ in range(options.number_of_devices)]
    else:
        S = options.service_times_per_visit
        V = [options.number_of_visits]

    # Initialization
    M, N = options.number_of_devices, options.number_of_users
    Z = options.thinking_time

    # Average number of jobs at the i-th device
    Q = [0 for _ in range(M)]

    # Response time of the i-th device
    R = [0 for _ in range(M)]

    # System throughput
    X_sys = 0

    # System response time
    R_sys = 0

    for n in range(1, N + 1):
        for i in range(M):
            R[i] = S[i] * (1 + Q[i])

        R = [S[i] for i in range(M)] if options.is_delay_center else [S[i] * (1 + Q[i]) for i in range(M)]

        R_sys = sum([R[t] * V[t] for t in range(M)])

        X_sys = n/(Z + R_sys)

        Q = [X_sys * V[t] * R[t] for t in range(M)]

    # Device throughputs
    X_ = [X_sys * V[t] for t in range(M)]

    # Device utilizations
    U = [X_sys * S[t] * V[t] for t in range(M)]

    # Write to JSON
    mapping = {'system_throughput': X_sys,
               'average_number_jobs_per_device': Q,
               'response_time_per_device': [1000 * item for item in R],
               'system_response_time': 1000 * (R_sys + Z),
               'utilization_per_device': U}

    print("N = " + str(N) + ", M = " + str(M) + ",  RT = " + str(1000 * (R_sys + Z)) + " ms, X = " + str(X_sys) + " Req./s")

    with open('mva.json', 'w') as file:
        json.dump(mapping, file, indent=4)

if __name__ == "__main__":
    main()