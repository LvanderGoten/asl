__author__ = 'lennart'
import matplotlib.pyplot as plt
from optparse import OptionParser


def main():
    # Parser for stdin
    parser = OptionParser()

    parser.add_option('--T',
                      help='the x-y mapping to plot (throughput)',
                      metavar='FILE')
    
    parser.add_option('--R',
                      help='the x-y mapping to plot (response time)',
                      metavar='FILE')

    (options, args) = parser.parse_args()

    # Extract Throughput
    X, T = [], []
    with open(options.T, 'r') as m:
        for line in m:
            x, y = line.rstrip('\n').split(' ')

            X += [float(x)]
            T += [float(y)]

    # Extract response time
    R = []
    with open(options.R, 'r') as p:
        for line in p:
            _, y = line.rstrip('\n').split(' ')

            R += [float(y)]

    # Plot points
    plt.figure()
    plt.subplot(211)
    plt.plot(X, T, linewidth=2.0)
    plt.scatter(X, T, s=100, c='r', marker="x", linewidths=2)

    # Axes
    plt.xlabel("Number of client jobs")
    plt.ylabel("Throughput (Req/sec)")
    plt.axis([0, 50, 0, 10000])
    plt.xticks(X)
    plt.grid()

    plt.subplot(212)
    plt.plot(X, R, linewidth=2.0)
    plt.scatter(X, R, s=100, c='r', marker="x", linewidths=2)
    plt.xlabel("Number of client jobs")
    plt.ylabel("Response time (msec)")
    plt.axis([0, 50, 0, 2])
    plt.xticks(X)

    plt.grid()
    plt.show()

if __name__ == "__main__":
    main()