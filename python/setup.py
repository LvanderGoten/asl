import sys
import subprocess
import pxssh
import time
from xml.dom import minidom
from optparse import OptionParser


def main():
    # Parser for stdin
    parser = OptionParser()

    parser.add_option('--compile_only',
                      help='whether we should only perform a local compile',
                      default=False,
                      action='store_true')

    parser.add_option('-d', '--dryad_id',
                      help='the targeted middleware dryad',
                      type='int',
                      metavar='ID')
    
    parser.add_option('-c', '--config',
                      help='the initialization xml',
                      metavar='CFG.xml')
    
    parser.add_option('-s', '--setup_db',
                      help='whether to setup the database',
                      default=False,
                      action='store_true')

    parser.add_option('-v', '--verbose',
                      help='whether to display more information',
                      default=False,
                      action='store_true')
    
    (options, args) = parser.parse_args()

    # Retrieve file name of initialization file (.xml)
    init_file = options.config

    # Dryad identifier
    dryad_id = '0' + str(options.dryad_id) if options.dryad_id < 10 else str(options.dryad_id)

    if options.verbose:
        s = pxssh.pxssh(logfile=sys.stdout.buffer, timeout=200)
    else:
        s = pxssh.pxssh(timeout=200)

    # Read initialization file
    xml_doc = minidom.parse(init_file)

    # Clean up temporaries
    print("[PY]: Cleaning up...")
    subprocess.run(["rm", "-r", "ant/build"])

    # Build client
    print("[PY]: Building client...")
    subprocess.run(["ant", "-q", "-buildfile", "ant/build_client.xml", "jar"])

    # Build server
    print("[PY]: Building server...")
    subprocess.run(["ant", "-q", "-buildfile", "ant/build_server.xml", "jar"])

    if options.compile_only:
        print("[PY]: Done")
        sys.exit(0)

    host = "dryad" + dryad_id + ".ethz.ch"

    if not s.login(host, "lennartv", ""):
        print("[PY]: SSH login failed!")
    else:
        print("[PY]: SSH connection established")

        # Setup DB
        if options.setup_db:
            item_list = xml_doc.getElementsByTagName('database')
            db_port = item_list[0].attributes['port'].value

            print("[PY]: Killing previous PostgreSQL instances...")
            s.sendline('/mnt/local/lennartv/postgres/bin/pg_ctl stop -D /mnt/local/lennartv/postgres/db')
            s.prompt()

            # Cleanup /mnt/local/lennartv
            s.sendline('rm -r /mnt/local/lennartv/*')
            s.prompt()

            print("[PY]: Uploading config files...")
            subprocess.run(["scp", "-r", "config/", "lennartv@dryad" + dryad_id + ".ethz.ch:/mnt/local/lennartv"])

            # Compile and install
            s.sendline('cd ~/postgresql-9.4.5/')
            s.prompt()
            print("[PY]: Configure build...")
            s.sendline('./configure --prefix="/mnt/local/lennartv/postgres"')
            s.prompt()
            print("[PY]: Executing 'make'...")
            s.sendline('make')
            s.prompt()
            print("[PY]: Installing build...")
            s.sendline('make install')
            s.prompt()
            s.sendline('LD_LIBRARY_PATH=/mnt/local/lennartv/postgres/lib')
            s.prompt()
            s.sendline('export LD_LIBRARY_PATH')
            s.prompt()
            print("[PY]: Initializing PostgreSQL...")
            s.sendline('/mnt/local/lennartv/postgres/bin/initdb -D /mnt/local/lennartv/postgres/db/')
            s.prompt()

            # Copy postgres.conf
            s.sendline('cp -f /mnt/local/lennartv/config/postgresql.conf /mnt/local/lennartv/postgres/db')
            s.prompt()

            print("[PY]: Starting PostgreSQL...")
            s.sendline('/mnt/local/lennartv/postgres/bin/postgres -D /mnt/local/lennartv/postgres/db/ -p '
                       + db_port
                       + ' -i -k /mnt/local/lennartv/ 2>&1 &')
            s.prompt()

            # Create database lennartv
            time.sleep(5)
            print("[PY]: Create database 'lennartv'...")
            s.sendline('/mnt/local/lennartv/postgres/bin/createdb -U lennartv -h localhost -p ' + db_port)

            # Upload DB scripts
            print("[PY]: Copying DB scripts...")
            subprocess.run(["scp", "-r", "database", "lennartv@dryad" + dryad_id + ".ethz.ch:/mnt/local/lennartv"])\


            # Execute database scripts
            time.sleep(5)
            print("[PY]: Executing DB scripts...")
            s.sendline('/mnt/local/lennartv/postgres/bin/psql -U lennartv -h localhost -p ' + db_port
                       + ' -d lennartv -a'
                       ' -f /mnt/local/lennartv/database/create_tables.sql')
            s.prompt()
            s.sendline('/mnt/local/lennartv/postgres/bin/psql -U lennartv -h localhost -p ' + db_port
                       + ' -d lennartv -a'
                       ' -f /mnt/local/lennartv/database/stored_procedures.sql')

            # Close SSH
            print("[PY]: Closing SSH connection...")
            s.prompt()
            s.logout()

        print("[PY]: Uploading JARs...")
        subprocess.run(["scp", "-r", "ant/build", "lennartv@dryad" + dryad_id + ".ethz.ch:/mnt/local/lennartv"])\
            
        print("[PY]: Done")


if __name__ == "__main__":
    main()