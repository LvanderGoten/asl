__author__ = 'lennart'
import os
import sys
import subprocess
import random
import datetime
import time
from optparse import OptionParser, OptionGroup


def chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i+n]


class Tee(object):
    def __init__(self, *files):
        self.files = files

    def write(self, obj):
        for f in self.files:
            f.write(obj)
            f.flush()

    def flush(self):
        for f in self.files:
            f.flush()


class Server:
    def __init__(self, server_addr, server_ssh_key, server_user, server_port=-1, server_psql_user=None):
        self.addr = server_addr
        self.ssh_key = server_ssh_key
        self.user = server_user
        self.port = server_port
        self.psql_user = server_psql_user


def send_command_ec2(server, cmd):
    addr, ssh_key, user = server.addr, server.ssh_key, server.user

    return subprocess.Popen(["ssh", "-i", ssh_key,
                             user + "@" + addr, cmd])


def random_name(n):
    # Adapted from
    # http://stackoverflow.com/questions/2257441/random-string-generation-with-upper-case-letters-and-digits-in-python
    return ''.join(random.choice([str(i) for i in range(1, 10)]) for _ in range(n))


def upload_to_ec2(server, handle_from,  handle_to):
    return subprocess.Popen(["scp", "-i", server.ssh_key,
                            "-r", handle_from,
                             server.user + "@" + server.addr + ":" + handle_to])


def create_sql_account(db_server, client_hash):
    sql_cmd = "SELECT createAccount('" + client_hash + "')"
    return send_command_ec2(db_server, "psql -U " + db_server.psql_user
                            + " -d " + db_server.psql_user + " -c \"" + sql_cmd + "\"")


def create_sql_queue(db_server, queue_id):
    sql_cmd = "SELECT createQueue(" + str(queue_id) + ")"
    return send_command_ec2(db_server, "psql -U " + db_server.psql_user
                            + " -d " + db_server.psql_user + " -c \"" + sql_cmd + "\"")


def reset_db(db_server):
    return send_command_ec2(db_server, "psql -U " + db_server.psql_user +
                            " -d " + db_server.psql_user + " -c \"SELECT resetTables()\"")


def fill_db(msg_size, fill_size, db_server):
    return send_command_ec2(db_server, "psql -U " + db_server.psql_user +
                            " -d " + db_server.psql_user
                            + " -c \"SELECT fillDb(" + str(msg_size) + ", " + str(fill_size) + ")\"")


def spawn_clients(client_nodes, middleware_nodes, jobs):
    handles = []

    J, K, N = len(jobs), len(client_nodes), len(middleware_nodes)

    job_indices = list(range(J))

    for job_group_indices in chunks(job_indices, K):

        # Draw job set
        job_set = [jobs[z] for z in job_group_indices]

        # Pick up to K middleware nodes uniformly at random
        random.shuffle(middleware_nodes)
        target_nodes = middleware_nodes[0:K]

        for k in range(len(job_set)):
            client_node = client_nodes[k]

            # Modular middleware node index
            z = k % N

            # Assemble instruction
            cmd = "java -Dhash=" + job_set[k].split('/')[1] + " "\
                  + "-jar ~/ASL/build_client/jar/ASL_Milestone_1_Client.jar "\
                  + target_nodes[z].addr + " "\
                  + str(target_nodes[z].port) + " "\
                  + "~/ASL/" + job_set[k]

            # Circumvent SIGHUP
            nohup_cmd = "nohup " + cmd + ">> error.log 2>> error.log < /dev/null &"
            stat = "ssh -i " + client_node.ssh_key + " " + client_node.user \
                   + "@" + client_node.addr + " " + "'" + nohup_cmd + "'"

            handles += [subprocess.Popen("ssh -i " + client_node.ssh_key + " "
                                         + client_node.user + "@" + client_node.addr + " "
                                         + "'" + nohup_cmd + "'", shell=True)]

            print("[EXEC]: [" + job_set[k]
                  + "] on client: [" + client_node.addr
                  + "] >> middleware node: [" + target_nodes[z].addr + "]")

    return handles


def parse_set_callback(option, opt, value, parser):
    setattr(parser.values, option.dest, value[1:-1].split(','))


def main():
    # Parser for stdin
    parser = OptionParser()

    client_group = OptionGroup(parser, "Client server settings",
                               "An arbitrary amount K of middleware nodes can be specified, "
                               "load is to be distributed equally")

    client_group.add_option('--client_addrs',
                            help='the targeted client server addresses (IPv4)',
                            type='string',
                            action='callback',
                            callback=parse_set_callback,
                            metavar='{ADDR_1, ..., ADDR_K}')

    client_group.add_option('--client_ssh_keys',
                            help='SSH keys of client servers',
                            metavar='{SSH_KEY_1, ..., SSH_KEY_K}',
                            default='EC2/m3_medium.pem',
                            type='string',
                            action='callback',
                            callback=parse_set_callback)

    client_group.add_option('--client_users',
                            help='Users of client server [default %default]',
                            metavar='{USER_1, ..., USER_K}',
                            default='ec2-user',
                            type='string',
                            action='callback',
                            callback=parse_set_callback)

    parser.add_option_group(client_group)

    middleware_group = OptionGroup(parser, "Middleware server settings",
                                   "An arbitrary amount N of middleware nodes can be specified, "
                                   "load is to be distributed equally")

    middleware_group.add_option('--middleware_addrs',
                                help='the targeted middleware addresses (IPv4)',
                                metavar='{ADDR_1, ..., ADDR_N}',
                                type='string',
                                action='callback',
                                callback=parse_set_callback)

    middleware_group.add_option('--middleware_ports',
                                help='the targeted middleware ports [default %default]',
                                metavar='{PORT_1, ..., PORT_N}',
                                default='36406',
                                type='string',
                                action='callback',
                                callback=parse_set_callback)

    middleware_group.add_option('--middleware_ssh_keys',
                                help='SSH keys of middleware servers',
                                metavar='{SSH_KEY_1, ..., SSH_KEY_N}',
                                default='EC2/m3_large.pem',
                                type='string',
                                action='callback',
                                callback=parse_set_callback)

    middleware_group.add_option('--middleware_users',
                                help='Users of middleware servers [default %default]',
                                metavar='{USER_1, ..., USER_N}',
                                default='ec2-user',
                                type='string',
                                action='callback',
                                callback=parse_set_callback)

    parser.add_option_group(middleware_group)

    database_group = OptionGroup(parser, "Database settings")

    database_group.add_option('--database_addr',
                              help='the targeted database address (IPv4)',
                              metavar='ADDR')

    database_group.add_option('--database_ssh_key',
                              help='SSH key of database server',
                              metavar='SSH_KEY',
                              default='EC2/c4_xlarge.pem')

    database_group.add_option('--database_user',
                              help='User of database server [default %default]',
                              metavar='USER',
                              default='ec2-user')

    database_group.add_option('--database_psql_user',
                              help='psql user of database server [default %default]',
                              metavar='USER',
                              default='postgres')

    parser.add_option_group(database_group)

    experiment_group = OptionGroup(parser, "Experiment parameters")

    experiment_group.add_option('--num_clients',
                                help='the number of participating clients',
                                metavar='NUM',
                                type='int',
                                default=30)

    experiment_group.add_option('--message_size',
                                help="the message payload size in characters [default %default]",
                                metavar='NUM',
                                type='int',
                                default=500)

    experiment_group.add_option('--duration',
                                help="the default time (secs) for an experiment [default %default]",
                                metavar='SECS',
                                type='int',
                                default=12 * 60)

    experiment_group.add_option('--client_delay',
                                help="the delay between user actions in ms [default %default]",
                                metavar='MSECS',
                                type='int',
                                default=0)

    experiment_group.add_option('--db_fill_count',
                                help="the number of messages that should be prestored in the DB [default %default]",
                                metavar='NUM',
                                type='int',
                                default=0)

    experiment_group.add_option('--memory_burst',
                                help="whether to avoid popping messages after sending them [default %default]",
                                default=False,
                                action="store_true")

    parser.add_option_group(experiment_group)

    miscellaneous_group = OptionGroup(parser, "Miscellaneous:")

    miscellaneous_group.add_option('--download_logs',
                                   help="whether to download client and middleware logs after spawning "
                                        "[default %default]",
                                   default=False,
                                   action="store_true")

    dir_path = "experiments_revised/" + datetime.datetime.now().isoformat() + "/"
    subprocess.run(["mkdir", "-p", dir_path])
    log_file = open(dir_path + "py.out", "w")
    sys.stdout = Tee(sys.stdout, log_file)
    print(sys.argv)

    parser.add_option_group(miscellaneous_group)

    (options, args) = parser.parse_args()
    nOfNodes = len(options.middleware_addrs)
    nOfClients = len(options.client_addrs)

    # Propagate defaults
    if not isinstance(options.client_ssh_keys, list):
        val = options.client_ssh_keys
        options.client_ssh_keys = [val for _ in range(nOfClients)]

    if not isinstance(options.client_users, list):
        val = options.client_users
        options.client_users = [val for _ in range(nOfClients)]

    if not isinstance(options.middleware_ports, list):
        val = options.middleware_ports
        options.middleware_ports = [val for _ in range(nOfNodes)]

    if not isinstance(options.middleware_ssh_keys, list):
        val = options.middleware_ssh_keys
        options.middleware_ssh_keys = [val for _ in range(nOfNodes)]

    if not isinstance(options.middleware_users, list):
        val = options.middleware_users
        options.middleware_users = [val for _ in range(nOfNodes)]

    # Cast port numbers
    options.middleware_ports = [int(port) for port in options.middleware_ports]

    # Create server objects
    client_nodes = []
    for j in range(nOfClients):
        client_nodes += [Server(options.client_addrs[j],
                                options.client_ssh_keys[j],
                                options.client_users[j])]

    middleware_nodes = []
    for i in range(nOfNodes):
        middleware_nodes += [Server(options.middleware_addrs[i],
                                    options.middleware_ssh_keys[i],
                                    options.middleware_users[i],
                                    options.middleware_ports[i])]

    database_server = Server(options.database_addr, options.database_ssh_key,
                             options.database_user, server_psql_user=options.database_psql_user)

    # Cleanup locally
    subprocess.run(["rm", "-r", "behavior/"])
    subprocess.run(["mkdir", "behavior/"])

    # Generate behavior
    behavior_lst = []

    # Reset database
    reset_db(database_server).wait()

    # Fill database (if required)
    if options.db_fill_count > 0:
        fill_db(options.message_size, options.db_fill_count, database_server).wait()

    with open('config/preamble', 'r') as f:
        preamble_text = f.read()

    for client_id in range(1, options.num_clients + 1):

        # Assign random name
        client_hash = random_name(8)

        file_name = 'behavior/' + str(client_id) + "__" + client_hash
        behavior_lst += [file_name]

        with open(file_name, 'w') as code:
            # Reserve ID in database
            create_sql_account(database_server, client_hash).wait()

            # Each client has its own queue
            queue_id = client_id

            # Create queue in database
            create_sql_queue(database_server, queue_id).wait()

            # Prepend preamble
            code.write(preamble_text)

            # Login client
            code.write('CA ' + client_hash + '\n')

            # Start instruction
            code.write('LOOP [')

            # Enqueue message
            code.write('<EQ ' + str(queue_id) + ' ' + str(client_id) + ' ' + str(options.message_size) + '>,')

            # Wait some time
            code.write('<WAIT ' + str(options.client_delay) + '>,')

            # Dequeue message (pop)
            if not options.memory_burst:
                code.write('<RQ_POP ' + str(queue_id) + '>')

            # End instruction
            code.write(']\n')

            # Disconnect client
            code.write('D\n')

    # Set up client servers (clean and upload files subsequently)
    handles = [('behavior', '~/ASL'),
               ('ant/build/build_client', '~/ASL')]

    procs = []
    for client_server in client_nodes:
        send_command_ec2(client_server, "rm -r ~/ASL").wait()
        send_command_ec2(client_server, "mkdir ~/ASL").wait()
        send_command_ec2(client_server, "rm -r ~/logs").wait()

        procs += [upload_to_ec2(client_server, handle[0], handle[1]) for handle in handles]

    # Wait for all to complete
    for proc in procs:
        proc.wait()

    # Spawn clients
    for spawned_client in spawn_clients(client_nodes, middleware_nodes, behavior_lst):
        spawned_client.wait()

    # Experiment time
    print("[PY]: Experiment time: " + str(options.duration) + " seconds...")
    time.sleep(options.duration)

    # Shutdown middleware nodes
    for middleware_node in middleware_nodes:
        send_command_ec2(middleware_node, "pkill java").wait()

    print("[PY]: Experiment completed (server shutdown)")

    if options.download_logs:
        print("[PY]: Downloading logs..")

        # Create subdirectories
        server_log_path = dir_path + "server/"
        client_log_path = dir_path + "client/"

        # Collect logs from middleware nodes
        for middleware_node in middleware_nodes:
            subdir_path = server_log_path + middleware_node.addr + "/"
            subprocess.run(["mkdir", "-p", subdir_path])

            subprocess.run(["scp",
                            "-i", middleware_node.ssh_key,
                            "-r",
                            middleware_node.user + "@" + middleware_node.addr + ":~/ASL/logs",
                            subdir_path])

        # Download configuration
        subprocess.run(["scp",
                        "-i", middleware_nodes[0].ssh_key,
                        middleware_nodes[0].user + "@" + middleware_nodes[0].addr + ":~/ASL/config/init.xml",
                        server_log_path])

        # Collect logs from client servers
        for client_node in client_nodes:
            subdir_path = client_log_path + client_node.addr + "/"
            subprocess.run(["mkdir", "-p", subdir_path])

            subprocess.run(["scp",
                            "-i", client_node.ssh_key,
                            "-r", client_node.user + "@" + client_node.addr + ":~/logs",
                            subdir_path])


if __name__ == "__main__":
    main()