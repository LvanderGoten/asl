__author__ = 'lennart'
from optparse import OptionParser
import sys
import numpy as np
import math
import json


def main():
    # print(sys.argv)

    # Parser for stdin
    parser = OptionParser()

    parser.add_option('--expected_service_rate',
                      help='the expected service rate that the system generates',
                      metavar='SECS/REQ',
                      type='float')

    parser.add_option('--expected_throughput',
                      help='the expected throughput that the system offers',
                      metavar='REQ./SECS',
                      type='float')

    parser.add_option('--number_of_servers',
                      help='the number of middleware nodes',
                      metavar='NUM',
                      type='int')

    parser.add_option('--max_index',
                      help='how many different probabilities should be calculated',
                      metavar='NUM',
                      type='int')

    (options, args) = parser.parse_args()

    m = options.number_of_servers
    mean_service_rate = options.expected_service_rate
    mean_throughput = options.expected_throughput

    # Mean service rate
    # mean_service_rate = 1/mean_service_time

    # Traffic intensity
    rho = mean_throughput/(m * mean_service_rate)

    if rho > 1:
        print("System is not considered stable!")
        sys.exit(1)

    # Probabilities of having j jobs in the system
    P_sys = []

    # Probability of having zero jobs in the system
    p0_term_1 = np.power(m * rho, m)/(math.factorial(m) * (1 - rho))
    p0_term_2 = sum([np.power(m * rho, n)/math.factorial(n) for n in range(1, m)])
    p0 = 1/(1 + p0_term_1 + p0_term_2)
    P_sys += [p0]

    # Probabilities of more than zero jobs
    for n in range(options.max_index):
        if n < m:
            p = p0 * np.power(m * rho, n)/math.factorial(n)
        else:
            p = p0 * np.power(rho, n) * np.power(m, m)/math.factorial(m)

        P_sys += [p]

    # Probability of queueing
    varrho = p0 * np.power(m * rho, m)/(math.factorial(m) * (1 - rho))

    # Mean number of jobs in system
    mean_jobs_in_system = m * rho + rho * varrho/(1 - rho)

    # Variance of number of jobs in system
    variance_jobs_in_system = m * rho + rho * varrho * ((1 + rho - rho * varrho)/np.power(1 - rho, 2) + m)

    # Mean number of jobs in queue
    mean_jobs_in_queue = rho * varrho/(1 - rho)

    # Variance of number of jobs in queue
    variance_jobs_in_queue = rho * varrho * (1 + rho - rho * varrho)/np.power(1 - rho, 2)

    # Average utilization of each server
    U = rho

    # TODO: Cumulative distribution function of response time

    # Mean response time
    mean_response_time = 1/mean_service_rate * (1 + varrho/(m * (1 - rho)))

    # Variance of response time
    variance_response_time = 1 / np.power(mean_service_rate, 2) * (
        1 + varrho * (2 - varrho) / (np.power(m, 2) * np.power(1 - rho, 2)))

    # TODO: Cumulative distribution function of waiting time

    # Mean waiting time
    mean_waiting_time = varrho/(m * mean_service_rate * (1 - rho))

    # Variance of waiting time
    variance_waiting_time = varrho * (2 - varrho)/(np.power(m * mean_throughput * (1 - rho), 2))

    # 90th percentile of waiting time
    percentile_90_waiting_time = mean_waiting_time/varrho * np.log(10 * varrho)

    mapping = {'mean_service_time': mean_waiting_time,
               'throughput': mean_throughput,
               'mean_service_rate': mean_service_rate,
               'traffic_intensity': rho,
               'mean_jobs_in_system': mean_jobs_in_system,
               'variance_jobs_in_system': variance_jobs_in_system,
               'mean_jobs_in_queue': mean_jobs_in_queue,
               'utilization': U,
               'variance_jobs_in_queue': variance_jobs_in_queue,
               'mean_response_time': mean_response_time,
               'variance_response_time': variance_response_time,
               'mean_waiting_time': mean_waiting_time,
               'variance_waiting_time': variance_waiting_time,
               'percentile_90_waiting_time': percentile_90_waiting_time}

    with open('mmm.json', 'w') as file:
        json.dump(mapping, file, indent=4)

    print("rho = " + str(rho) + ", R = " + str(1000 * mean_response_time) + " ms"
          + " +- " + str(np.sqrt(variance_response_time)) + " ms")


if __name__ == "__main__":
    main()