__author__ = 'lennart'
from optparse import OptionParser
import numpy as np
import sys
import json


def main():
    print(sys.argv)

    # Parser for stdin
    parser = OptionParser()

    parser.add_option('--expected_service_rate',
                      help='the expected service rate that the system generates (without queueing)',
                      metavar='MSECS',
                      type='float')

    parser.add_option('--expected_throughput',
                      help='the expected throughput that the system offers',
                      metavar='REQ./SECS',
                      type='float')

    parser.add_option('--max_index',
                      help='how many different probabilities should be calculated',
                      metavar='NUM',
                      type='int')

    (options, args) = parser.parse_args()

    mean_service_rate, mean_throughput = options.expected_service_rate, options.expected_throughput
    N = options.max_index

    # Probabilities of having j jobs in the system
    P_system = []

    # Probabilities of having k jobs in the queue
    P_queue = []

    # Mean service rate
    # mean_service_rate = 1/mean_service_time

    # Traffic intensity
    traffic_intensity = mean_throughput/mean_service_rate

    if traffic_intensity >= 1:
        print("System is not stable! traffic_intensity => " + str(traffic_intensity))
        sys.exit(1)

    # 5)
    for n in range(N + 1):
        P_system += [(1 - traffic_intensity) * np.power(traffic_intensity, n)]

    # Mean number of jobs in system
    mean_jobs_in_system = traffic_intensity/(1 - traffic_intensity)

    # Variance of number of jobs in system
    variance_jobs_in_system = traffic_intensity/(1 - traffic_intensity)**2

    # 8)
    P_queue += [1 - traffic_intensity**2]
    for k in range(1, N + 1):
        P_queue += [(1 - traffic_intensity) * np.power(traffic_intensity, k + 1)]

    # Mean number of jobs in queue
    mean_jobs_in_queue = traffic_intensity**2/(1 - traffic_intensity)

    # Variance of number of jobs in queue
    variance_jobs_in_queue = traffic_intensity**2 * (1 + traffic_intensity - traffic_intensity**2) / (1 - traffic_intensity)**2

    # Mean response time (Little's law)
    mean_response_time = 1/(mean_service_rate * (1 - traffic_intensity))

    # Variance response time
    variance_response_time = 1/(mean_service_rate * (1 - traffic_intensity))**2

    # Mean waiting time
    mean_waiting_time = traffic_intensity * mean_response_time

    # Variance of waiting time
    variance_waiting_time = (2 - traffic_intensity) * traffic_intensity/(mean_service_rate * (1 - traffic_intensity))**2

    # 90th percentile of waiting time
    percentile_90_waiting_time = max(0, mean_waiting_time/traffic_intensity * np.log(10 * traffic_intensity))

    mapping = {'mean_service_time': mean_waiting_time,
               'throughput': mean_throughput,
               'mean_service_rate': mean_service_rate,
               'traffic_intensity': traffic_intensity,
               'mean_jobs_in_system': mean_jobs_in_system,
               'variance_jobs_in_system': variance_jobs_in_system,
               'mean_jobs_in_queue': mean_jobs_in_queue,
               'variance_jobs_in_queue': variance_jobs_in_queue,
               'mean_response_time': mean_response_time,
               'variance_response_time': variance_response_time,
               'mean_waiting_time': mean_waiting_time,
               'variance_waiting_time': variance_waiting_time,
               'percentile_90_waiting_time': percentile_90_waiting_time,
               'probs_jobs_in_system': P_system,
               'probs_jobs_in_queue': P_queue}

    with open('mm1.json', 'w') as file:
        json.dump(mapping, file, indent=4)

if __name__ == "__main__":
    main()