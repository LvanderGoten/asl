import subprocess
from xml.dom import minidom
from optparse import OptionParser


def upload_to_ec2(options, handle_from,  handle_to):
    return subprocess.Popen(["scp", "-i", options.middleware_ssh_key,
                            "-r", handle_from,
                             options.middleware_user + "@" + options.middleware_addr + ":" + handle_to])


def send_command_ec2(options, cmd):
    return subprocess.Popen(["ssh", "-i", options.middleware_ssh_key,
                             options.middleware_user + "@" + options.middleware_addr,
                             cmd])

# GUIDELINES for EC2 SERVER SETUP:
# Needed tools: sudo yum install java wget vim
# HTOP: mkdir software; cd software; wget http://pkgs.repoforge.org/htop/htop-1.0.3-1.el7.rf.x86_64.rpm
# sudo rpm -ihv htop-1.0.3-1.el7.rf.x86_64.rpm
# SSH: /etc/ssh/sshd_config change MaxStartups 500
# Open ports 36406 (MessageIntermediator) and (or) 5432 (PostgreSQL)
# Reboot machine


def main():
    # Parser for stdin
    parser = OptionParser()

    parser.add_option('--middleware_addr',
                      help='the targeted middleware address (IPv4)',
                      metavar='ADDR')

    parser.add_option('--middleware_ssh_key',
                      help='SSH key of middleware',
                      metavar='SSH_KEY',
                      default='EC2/m3_large.pem')

    parser.add_option('--middleware_user',
                      help='SSH key of middleware',
                      metavar='SSH_KEY',
                      default='ec2-user')

    parser.add_option('--config',
                      help='the initialization xml',
                      metavar='CFG.xml')
    
    parser.add_option('--setup_db',
                      help='whether to setup the database',
                      default=False,
                      action='store_true')

    parser.add_option('--verbose',
                      help='whether to display more information',
                      default=False,
                      action='store_true')

    parser.add_option('--timeout',
                      help='how long the SSH unit should wait before a teardown',
                      type='int',
                      default=200)
    
    (options, args) = parser.parse_args()

    # Retrieve file name of initialization file (.xml)
    init_file = options.config

    # Read initialization file
    xml_doc = minidom.parse(init_file)

    # Clean up temporaries (local)
    print("[PY]: Cleaning up...")
    subprocess.run(["rm", "-r", "ant/build"])

    # Build client
    print("[PY]: Building client...")
    subprocess.run(["ant", "-q", "-buildfile", "ant/build_client.xml", "jar"])

    # Build server
    print("[PY]: Building server...")
    subprocess.run(["ant", "-q", "-buildfile", "ant/build_server.xml", "jar"])

    # Clean up temporaries (server)
    send_command_ec2(options, "rm -r ~/ASL").wait()
    send_command_ec2(options, "mkdir ~/ASL").wait()

    # Upload configuration files (async)
    handles = [('config', '~/ASL'),
               ('ant', '~/ASL'),
               ('database', '~/ASL')]

    procs = [upload_to_ec2(options, handle[0], handle[1]) for handle in handles]

    # Wait for everything to complete
    for proc in procs:
        proc.wait()

    if options.setup_db:
        send_command_ec2(options, "sudo service postgresql restart")

        # TODO: PSQL User
        send_command_ec2(options, "psql -U other_user -d other_user -f ~/ASL/database/create_tables.sql").wait()
        send_command_ec2(options, "psql -U other_user -d other_user -f ~/ASL/database/stored_procedures.sql").wait()



if __name__ == "__main__":
    main()