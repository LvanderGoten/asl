__author__ = 'lennart'
import json
import statistics
from optparse import OptionParser
import matplotlib.pyplot as plt
import numpy as np


def parse_set_callback(option, _, value, parser):
    setattr(parser.values, option.dest, value[1:-1].split(','))


class ComparisonChart:
    def __init__(self, max_y, ylabel, legend, marker, color):
        self.max_y = max_y
        self.ylabel = ylabel
        self.legend = legend
        self.marker = marker
        self.color = color

        self.data = []

    def add_value(self, y):
        self.data += [y]


class TimeChart:
    def __init__(self, max_y, ylabel, legend, fmt):
        self.max_y = max_y
        self.ylabel = ylabel
        self.legend = legend
        self.fmt = fmt

        self.data = []
        self.error_values = []
        self.error_caption = ''
        self.error_above = False

    def add_sample(self, x, y):
        self.data += [x, y]

    def add_error_sample(self, dy, caption, above):
        self.error_values = dy
        self.error_caption = caption
        self.error_above = above


def main():
    # Parser for stdin
    parser = OptionParser()

    parser.add_option('--config',
                      help='the json describing how and which data should be plotted',
                      metavar='JSON')

    parser.add_option('--show_plots',
                      default=False,
                      action='store_true')

    (options, args) = parser.parse_args()

    # Lists of plots
    L = []
    with open(options.config, 'r') as cfg:
        config = json.load(cfg)

        duration = config['duration']
        output_dir = config['output_dir']

        for input_file in config['experiments']:
            with open(input_file, 'r') as fp:
                L += [json.load(fp)]

    comparison_charts = []
    for item in config['comparison_charts']['charts']:
        comparison_chart = ComparisonChart(item['max_y'], item['ylabel'],
                                           item['legend'], item['marker'], item['color'])

        # Add data points
        for k in range(len(L)):
            y = L[k][item['quantity']]

            comparison_chart.add_value(y)

        comparison_charts += [comparison_chart]

    time_charts = []
    for item in config['time_charts']['charts']:
        time_chart = TimeChart(item['max_y'], item['ylabel'],
                               item['legend'], item['format'])

        # Add data points
        x = L[item['experiment_id']]['plots']['X']
        y = L[item['experiment_id']]['plots'][item['quantity']]

        if 'errorbar' in item:
            quantity = item['errorbar']['quantity']
            dy = L[item['experiment_id']]['plots'][quantity]
            above = False

            if item['errorbar']['subtractive']:
                dy = [dy[z] - y[z] for z in range(len(y))]
                above = True

            err_caption = item['errorbar']['caption']
            time_chart.add_error_sample(dy, err_caption, above)

        time_chart.add_sample(x, y)
        time_charts += [time_chart]

    stats = dict()
    # Generate plots
    for task in config['generate']:
        output = task['output']
        plot_indices = task['indices']

        plt.figure()
        if task['type'] == 'time_chart':
            for plot_index in plot_indices:
                time_chart = time_charts[plot_index]
                x, y = time_chart.data
                max_x = duration

                if time_chart.error_values:
                    plt.errorbar(x, y, yerr=time_chart.error_values, lolims=True, errorevery=2,
                                 capsize=1, linewidth=2, elinewidth=1,
                                 label=time_chart.legend + time_chart.error_caption)
                else:
                    plt.plot(x, y, time_chart.fmt, linewidth=2, label=time_chart.legend)

            plt.axis([0, duration, 0, time_chart.max_y])
            xlabel = config['time_charts']['xlabel']
            ylabel = time_chart.ylabel
        elif task['type'] == 'comparison_chart':
            for plot_index in plot_indices:
                comparison_chart = comparison_charts[plot_index]
                max_x = config['comparison_charts']['max_x']
                plt.scatter(config['comparison_charts']['indices'], comparison_chart.data,
                            marker=comparison_chart.marker, c=comparison_chart.color)
                plt.plot(config['comparison_charts']['indices'], comparison_chart.data, c=comparison_chart.color,
                         label=comparison_chart.legend)

                plt.axis([0, max_x, 0, comparison_chart.max_y])
                # plt.xticks(np.linspace(0, config['comparison_charts']['max_x'], 10))
                # plt.xticks(range(config['comparison_charts']['max_x'] + 1))
                xlabel = config['comparison_charts']['xlabel']
                ylabel = comparison_chart.ylabel

                if 'stats' in task:
                    stats[comparison_chart.legend] = comparison_chart.data

            print("")

        # Add an supplementary line
        if 'other' in task:
            supp = task['other']
            if supp['type'] == "constant":
                lvalue, rvalue = supp['lvalue'], supp['rvalue']
                color = supp['color']
                plt.plot([0, max_x], [lvalue, rvalue], color=color,
                         label=supp['label'])
            elif supp['type'] == "point_set":
                other_x, other_y = supp['x'], supp['y']
                color = supp['color']
                plt.scatter(other_x, other_y, color=color)
                plt.plot(other_x, other_y, color=color, label=supp['label'])

        # Save statistics file
        if 'stats' in task:
            with open(output_dir + "stats.json", "w") as fp:
                json.dump(stats, fp, indent=2)


        plt.legend()
        plt.xlabel(xlabel, fontsize=20)
        plt.ylabel(ylabel, fontsize=23)
        plt.grid()
        plt.savefig(output_dir + output, bbox_inches='tight')

    if options.show_plots:
        plt.show()

if __name__ == "__main__":
    main()