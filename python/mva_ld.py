__author__ = 'lennart'
from optparse import OptionParser
import json
import sys


def parse_set_callback_string(option, _, value, parser):
    setattr(parser.values, option.dest, value[1:-1].split(','))


def parse_set_callback_float(option, _, value, parser):
    setattr(parser.values, option.dest, [float(item) for item in value[1:-1].split(',')])


def parse_matrix(option, _, value, parser):
    rows = value[1:-1].split(';')
    mat = []

    for row in rows:
        elements = row[1:-1].split(',')
        mat_row = []

        for element in elements:
            number = float(element)
            mat_row += [number]

        mat += [mat_row]

    setattr(parser.values, option.dest, mat)


def main():
    # Parser for stdin
    parser = OptionParser()

    parser.add_option('--thinking_time',
                      help='the time the clients think',
                      metavar='MS',
                      type='float')

    parser.add_option('--service_time_per_visit',
                      help='service time per visit to the i-th device',
                      type='string',
                      action='callback',
                      callback=parse_set_callback_float,
                      metavar='[S_1,...,S_M]')

    parser.add_option('--number_of_visits',
                      help='Number of visits to the i-th device',
                      type='string',
                      action='callback',
                      callback=parse_set_callback_float,
                      metavar='[V_1,...,V_M]')

    parser.add_option('--number_of_devices',
                      help='Number of devices (=M)',
                      metavar='NUM',
                      type='int')

    parser.add_option('--number_of_users',
                      help='Number of users (=N)',
                      metavar='NUM',
                      type='int')

    parser.add_option('--center_service_rates_per_job',
                      help='service rate of the i-th center when there are j jobs in it',
                      type='string',
                      action='callback',
                      callback=parse_matrix,
                      metavar='[[µ_11,...,µ_1N];...;[µ_M1,...,µ_MN]]')

    parser.add_option('--center_types',
                      help="the type of the service center (fixed capacity, delay center, load-dependent)",
                      type='string',
                      action='callback',
                      callback=parse_set_callback_string,
                      metavar='[t_1,...,t_M] where t_i ∈ {fc,dc,ld}')

    (options, args) = parser.parse_args()

    Z = options.thinking_time
    S = options.service_time_per_visit
    V = options.number_of_visits
    M = options.number_of_devices
    N = options.number_of_users
    mu = options.center_service_rates_per_job
    T = options.center_types

    # Input checks
    if len(V) != M or len([item for sublist in mu for item in sublist]) != M * N:
        print("Check input!")
        sys.exit(1)

    # Adapt indices
    S = [-1] + S
    V = [-1] + V
    T = [''] + T
    mu = [[-1 for _ in range(N)]] + mu
    mu = [[-1] + row for row in mu]

    # Initialization
    P = [[-1 for _ in range(N + 1)] for _ in range(M + 1)]
    Q = [-1 for _ in range(M + 1)]

    for k in range(1, M + 1):
        if T[k] in ['fc', 'dc']:
            Q[k] = 0
        elif T[k] == 'ld':
            P[k][0] = 1
        else:
            print("Wrong type!")
            sys.exit(1)

    R = [0 for _ in range(M + 1)]
    X_sys = 0

    for n in range(1, N + 1):
        for i in range(1, M + 1):
            if T[i] == 'fc':
                R[i] = S[i] * (1 + Q[i])
            elif T[i] == 'dc':
                R[i] = S[i]
            else:
                R[i] = sum([P[i][j - 1] * j/mu[i][j] for j in range(1, n + 1)])

        R_sys = sum([R[k] * V[k] for k in range(1, M + 1)])
        X_sys = n/(Z + R_sys)

        for i in range(1, M + 1):
            if T[i] in ['fc', 'dc']:
                Q[i] = X_sys * V[i] * R[i]
            else:
                for j in range(n, 0, -1):
                    P[i][j] = X_sys/mu[i][j] * P[i][j - 1]

                P[i][0] = 1 - sum(P[i][1:n+1])

    X = [X_sys * V[i] for i in range(1, M + 1)]

    U = [-1 for _ in range(M + 1)]
    for i in range(1, M + 1):
        if T[i] in ['fc', 'dc']:
            U[i] = X_sys * S[i] * V[i]
        else:
            U[i] = 1 - P[i][0]

    print("OK")
    mapping = {
        'system_throughput': X_sys,
        'throughput': X,
        'average_number_of_jobs': Q[1:],
        'response_time': R[1:],
        'system_response_time': R_sys,
        'utilization': U[1:],
        'job_probabilities': P[1:]
    }

    with open('mva_ld.json', 'w') as fp:
        json.dump(mapping, fp, indent=1)

if __name__ == "__main__":
    main()