__author__ = 'lennart'
import subprocess
import sys
import statistics
import matplotlib.pyplot as plt
from subprocess import PIPE
from optparse import OptionParser
from datetime import datetime, timedelta
import numpy as np
import json


def standardize(L, N):
    L_star = []

    for l in L:
        k = len(l)

        if k < N:
            fill_in = N - k
            last_el = l[-1] if k > 0 else 0
            l += [last_el for _ in range(fill_in)]
            L_star += [l]
        elif k == N:
            L_star += [l]
        else:
            L_star += [l[0:N]]

    return L_star


def split_into_subintervals(data, duration, total_duration):
    time_format = "[%H:%M:%S.%f]"
    N = len(data)

    if N == 0:
        return []

    subsets = []
    a = datetime.strptime(data[0].split(' ')[0], time_format)
    origin = a
    b = a

    i = 0
    while True:
        subset = []

        while (b - a).total_seconds() <= duration and i < N and (b - origin).total_seconds() <= total_duration:
            timestamp, quantity = data[i].split(' ')

            b = datetime.strptime(timestamp, time_format)
            x = (b - origin).total_seconds()
            subset += [(x, int(quantity)/1000)]

            i += 1

        subsets += [subset]
        if i < N and (b - origin).total_seconds() <= total_duration:
            a = b
        else:
            break

    return subsets


def main():
    # Parser for stdin
    parser = OptionParser()

    parser.add_option('--log_dir',
                      help='the log directory that contains the clients timing logs',
                      metavar='DIR')

    parser.add_option('--time',
                      help='the time to plot',
                      metavar='MINS',
                      default=10 * 60)

    parser.add_option('--time_slot',
                      help='the time slot interval length to plot',
                      metavar='SECS',
                      default=5)

    (options, args) = parser.parse_args()

    # Add trailing slash if inexistent
    options.log_dir = options.log_dir + '/' if options.log_dir[-1] != '/' else options.log_dir

    # Append all timing files and sort them

    # Find all log files
    find_proc = subprocess.Popen(["find", options.log_dir,
                                  "!", "-name", "clientEvents.log",
                                  "-name", "*.log", "-print0"], stdout=PIPE)

    # Delegate to cat
    xargs_proc = subprocess.Popen(["xargs", "-0", "cat"], stdin=find_proc.stdout, stdout=PIPE)

    # Sort by ascending time
    sort_proc = subprocess.Popen(["sort", "-d"], stdin=xargs_proc.stdout, stdout=PIPE)

    unfiltered = [x.decode('utf-8').rstrip('\n') for x in sort_proc.stdout]

    # Construct response time
    response_time = [x.split(' ')[0] + ' ' + x.split(' ')[2] for x in unfiltered if '[RESPONSE_TIME]' in x]

    # Construct setup time
    setup_time = [x.split(' ')[0] + ' ' + x.split(' ')[2] for x in unfiltered if '[SETUP_TIME]' in x]

    # Construct database response time
    db_response_time = [x.split(' ')[0] + ' ' + x.split(' ')[2] for x in unfiltered if '[DB_RESPONSE_TIME]' in x]

    # Construct service time
    service_time = [x.split(' ')[0] + ' ' + x.split(' ')[2] for x in unfiltered if '[SERVICE_TIME]' in x]

    # Construct db wait time
    db_wait_time = [x.split(' ')[0] + ' ' + x.split(' ')[2] for x in unfiltered if '[DB_WAIT_TIME]' in x]

    del unfiltered

    # Timing
    time_format = "[%H:%M:%S.%f]"
    a = datetime.strptime(response_time[0].split(' ')[0], time_format)
    b = a + timedelta(seconds=options.time)
    # b = datetime.strptime(response_time[-1].split(' ')[0], time_format)

    # Graphs
    X, R, T = [], [], []
    Setup_T = []
    DB_R_T, Serv_T, DB_Wait_T = [], [], []
    Serv_X = []
    User_Proj = []
    Cycle_R = []

    # Deviations
    Dev_R, Dev_Setup = [], []
    Dev_DB_R, Dev_Serv = [], []
    Dev_Cycle_R = []

    global_response_time_bucket = []
    response_time_buckets = split_into_subintervals(response_time, options.time_slot, options.time)

    print("Response times...")
    for response_time_bucket in response_time_buckets:
        # Write value to left end
        x = response_time_bucket[0][0]
        duration = response_time_bucket[-1][0] - response_time_bucket[0][0]

        XX, YY = zip(*response_time_bucket)

        # Statistics
        avg_response_time = statistics.mean(YY)

        # Percentiles
        percentile_90_response_time = np.percentile(YY, 90)

        # Throughput
        inter_throughput = len(response_time_bucket)/duration

        # Add to graphs
        X += [x]
        R += [avg_response_time]
        T += [inter_throughput]
        Dev_R += [percentile_90_response_time]

        global_response_time_bucket += list(YY)

    del response_time_buckets

    setup_time_buckets = split_into_subintervals(setup_time, options.time_slot, options.time)

    print("Setup times...")
    for setup_time_bucket in setup_time_buckets:
        x = setup_time_bucket[0][0]

        XX, YY = zip(*setup_time_bucket)

        # Statistics
        avg_setup_time = statistics.mean(YY)

        # Percentiles
        percentile_90_setup_time = np.percentile(YY, 90)

        # Add to graphs
        Setup_T += [avg_setup_time]
        Dev_Setup += [percentile_90_setup_time]

    del setup_time_buckets

    db_response_time_buckets = split_into_subintervals(db_response_time, options.time_slot, options.time)

    print("DB response times...")
    global_db_response_time_bucket = []
    for db_response_time_bucket in db_response_time_buckets:
        XX, YY = zip(*db_response_time_bucket)

        # Statistics
        avg_db_response_time = statistics.mean(YY)

        # Percentiles
        percentile_90_db_response_time = np.percentile(YY, 90)

        # Add to graph
        DB_R_T += [avg_db_response_time]
        Dev_DB_R += [percentile_90_db_response_time]

        global_db_response_time_bucket += YY

    del db_response_time_buckets

    service_time_buckets = split_into_subintervals(service_time, options.time_slot, options.time)

    print("Service times...")
    global_service_time_bucket = []
    for service_time_bucket in service_time_buckets:
        XX, YY = zip(*service_time_bucket)
        duration = service_time_bucket[-1][0] - service_time_bucket[0][0]

        # Statistics
        avg_service_time = statistics.mean(YY)

        # Percentiles
        percentile_90_service_time = np.percentile(YY, 90)

        # Add to graph
        Serv_T += [avg_service_time]
        Dev_Serv += [percentile_90_service_time]

        Serv_X += [len(service_time_bucket)/duration]

        global_service_time_bucket += YY

    del service_time_buckets

    print("DB wait times...")
    db_wait_buckets = split_into_subintervals(db_wait_time, options.time_slot, options.time)
    global_db_wait_time_bucket = []
    for db_wait_bucket in db_wait_buckets:
        XX, YY = zip(*db_wait_bucket)

        # Statistics
        avg_db_wait_time = statistics.mean(YY)

        # Add to graph
        DB_Wait_T += [avg_db_wait_time]

        global_db_wait_time_bucket += YY

    del db_wait_buckets

    # Standardize graphs to same lengths
    ref_N = len(X)
    L = [R, T, Setup_T, DB_R_T, Serv_T, Serv_X, Dev_R, Dev_Setup, Dev_DB_R, Dev_Serv, DB_Wait_T]
    R, T, Setup_T, DB_R_T, Serv_T, Serv_X, Dev_R, Dev_Setup, Dev_DB_R, Dev_Serv, DB_Wait_T = standardize(L, ref_N)

    # Cycle response time + user projections
    for z in range(ref_N):
        comb = R[z] + Setup_T[z]
        Cycle_R += [comb]
        User_Proj += [comb/1000 * T[z]]
        Dev_Cycle_R += [Dev_R[z] + Dev_Setup[z]]

    # Plots
    plt.figure()
    plt.plot(X, T, linewidth=2.0)
    plt.axis([0, options.time, 0, 10000])
    plt.xlabel("Time [sec]", fontsize=20)
    plt.ylabel("Throughput [Req/sec]", fontsize=23)
    plt.grid()
    plt.savefig(options.log_dir + "/throughput_plot.png", bbox_inches='tight')

    plt.figure()
    plt.plot(X, Serv_X, linewidth=2.0)
    plt.axis([0, options.time, 0, 10000])
    plt.xlabel("Time [sec]", fontsize=20)
    plt.ylabel("Service rate [Req/sec]", fontsize=23)
    plt.grid()
    plt.savefig(options.log_dir + "/service_rate_plot.png", bbox_inches='tight')

    plt.figure()
    plt.plot(X, Cycle_R, linewidth=2.0)
    plt.plot(X, Dev_Cycle_R,  color='#ffa500', linewidth=2.0, linestyle=':')
    plt.legend(["Response time"])
    plt.xlabel("Time [sec]", fontsize=20)
    plt.ylabel("Response time [ms]", fontsize=23)
    plt.axis([0, options.time, 0, 100])
    plt.grid()
    plt.savefig(options.log_dir + "/overall_response_time_plot.png", bbox_inches='tight')

    plt.figure()
    plt.plot(X, R, linewidth=2.0)
    plt.plot(X, Dev_R,  color='#ffa500', linewidth=2.0, linestyle=':')
    plt.legend(["Response time", "90$^{th}$ percentile"])
    plt.xlabel("Time [sec]", fontsize=20)
    plt.ylabel("Response time [ms]", fontsize=23)
    plt.axis([0, options.time, 0, 100])
    plt.grid()
    plt.savefig(options.log_dir + "/response_time_plot.png", bbox_inches='tight')

    plt.figure()
    plt.plot(X, Setup_T, linewidth=2.0)
    plt.plot(X, Dev_Setup,  color='#ffa500', linewidth=2.0, linestyle=':')
    plt.legend(["Setup time", "50$^{th}$ percentile (median)"])
    plt.xlabel("Time [sec]", fontsize=20)
    plt.ylabel("Setup time [ms]", fontsize=23)
    plt.axis([0, options.time, 0, 100])
    plt.grid()
    plt.savefig(options.log_dir + "/setup_time_plot.png", bbox_inches='tight')

    plt.figure()
    plt.plot(X, User_Proj, linewidth=2.0)
    plt.xlabel("Time [sec]", fontsize=20)
    plt.ylabel("Projected users", fontsize=23)
    plt.axis([0, options.time, 0, 130])
    plt.grid()
    plt.savefig(options.log_dir + "/user_proj_plot.png", bbox_inches='tight')

    plt.figure()
    plt.plot(X, DB_R_T, linewidth=2.0)
    plt.plot(X, Dev_DB_R,  color='#ffa500', linewidth=2.0, linestyle=':')
    plt.legend(["DB response time", "90$^{th}$ percentile"])
    plt.xlabel("Time [sec]", fontsize=20)
    plt.ylabel("Database query time [msec]", fontsize=23)
    plt.axis([0, options.time, 0, 100])
    plt.grid()
    plt.savefig(options.log_dir + "/db_resp_time_plot.png", bbox_inches='tight')

    plt.figure()
    plt.plot(X, Serv_T, linewidth=2.0)
    plt.plot(X, Dev_Serv,  color='#ffa500', linewidth=2.0, linestyle=':')
    plt.legend(["Service time", "90$^{th}$ percentile"])
    plt.xlabel("Time [sec]", fontsize=20)
    plt.ylabel("Service time [msec]", fontsize=23)
    plt.axis([0, options.time, 0, 100])
    plt.grid()
    plt.savefig(options.log_dir + "/service_time_plot.png", bbox_inches='tight')

    plt.figure()
    plt.plot(X, DB_Wait_T, linewidth=2.0)
    plt.xlabel("Time [sec]", fontsize=20)
    plt.ylabel("DB wait time [ms]", fontsize=23)
    plt.axis([0, options.time, 0, 0.01])
    plt.grid()
    plt.savefig(options.log_dir + "/db_wait_time_plot.png", bbox_inches='tight')

    # Histograms
    H_setup = []
    setup_filtered_resp_time = [line.rstrip('\n') for line in setup_time]
    if setup_filtered_resp_time:
        setup_filtered_resp_time.pop()
    for setup_line in setup_filtered_resp_time:
        _, setup_time = setup_line.split(' ')

        H_setup += [int(setup_time)/1000.0]

    # Create Statistics file
    with open(options.log_dir + "stat.json", 'w') as sta:
        duration = X[-1] - X[0]
        avg_throughput = len(global_response_time_bucket)/duration
        max_throughput = max(T)
        median_throughput = statistics.median(T)

        avg_cycle_resp = statistics.mean(Cycle_R)
        med_cycle_resp = statistics.median(Cycle_R)
        min_response_time_cyclic = min(Cycle_R)
        response_time_cyclic_stddev = statistics.stdev(Cycle_R)

        if global_service_time_bucket:
            avg_serv = statistics.mean(global_service_time_bucket)
        else:
            avg_serv = 0

        if global_db_response_time_bucket:
            avg_db_resp = statistics.mean(global_db_response_time_bucket)
            med_db_resp = statistics.median(global_db_response_time_bucket)
        else:
            avg_db_resp = 0
            med_db_resp = 0

        prod = avg_cycle_resp/1000.0 * avg_throughput

        if global_db_wait_time_bucket:
            avg_db_wait_time = statistics.mean(global_db_wait_time_bucket)
            med_db_wait_time = statistics.median(global_db_wait_time_bucket)
        else:
            avg_db_wait_time = 0
            med_db_wait_time = 0

        plots = {'number_of_points': ref_N,
                 'X': X,
                 'throughput': T,
                 'cyclic_response_time': Cycle_R,
                 'cyclic_response_time_dev': Dev_Cycle_R,
                 'user_projection': User_Proj,
                 'db_response_time': DB_R_T,
                 'db_response_time_dev': Dev_DB_R,
                 'service_time': Serv_T,
                 'service_time_dev': Dev_Serv
                 }

        mapping = {'throughput': avg_throughput,
                   'max_throughput': max_throughput,
                   'median_throughput': median_throughput,
                   'response_time_cyclic_min': min_response_time_cyclic,
                   'response_time_cyclic_average': avg_cycle_resp,
                   'response_time_cyclic_median': med_cycle_resp,
                   'response_time_cyclic_stddev': response_time_cyclic_stddev,
                   'service_time_average': avg_serv,
                   'db_response_time_average': avg_db_resp,
                   'db_response_time_median': med_db_resp,
                   'db_wait_time_average': avg_db_wait_time,
                   'db_wait_time_median': med_db_wait_time,
                   'product': prod,
                   'plots': plots,
                   'duration': duration}

        json.dump(mapping, sta, indent=4)

    print("Done")

if __name__ == "__main__":
    main()