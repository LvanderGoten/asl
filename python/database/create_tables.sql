-- Login: psql -U lennart -d ASL
-- Execution: psql -U lennart -d ASL -a -f /home/lennart/ETH/ASL/Project/Batch/setup_db.sql 

-- Clean up
DROP TABLE IF EXISTS Messages CASCADE;
DROP TABLE IF EXISTS Queues CASCADE;
DROP TABLE IF EXISTS Account CASCADE;

-- Setup structure for accounts 
CREATE TABLE Accounts (
	id SERIAL PRIMARY KEY,
 	name VARCHAR(20) NOT NULL
);

-- Add global entity
INSERT INTO Accounts (id, name)
VALUES (-1, 'Global');

-- Setup structure for queues (just keep the indices)
CREATE TABLE Queues (
	id SERIAL PRIMARY KEY, -- Queue index
	creatorId INTEGER REFERENCES Accounts(id), -- Initial creator
	timestamp TIMESTAMP DEFAULT current_timestamp -- Timestamp upon creation
);

INSERT INTO Queues(id, creatorId) VALUES (-1, -1);

-- Messages 
CREATE TABLE Messages (
	id SERIAL PRIMARY KEY, -- Message index
	queue_id INTEGER REFERENCES Queues(id) ON DELETE CASCADE, -- Queue where message resides
	sender_id INTEGER REFERENCES Accounts(id), -- Sender account ID
	receiver_id INTEGER REFERENCES Accounts(id), -- Receiver account ID (or -1 if message is intended to be public)
	message TEXT, -- Actual message
	timestamp TIMESTAMP DEFAULT current_timestamp -- Timestamp as registered upon insertion
);
