-- Peek a topmost message for a given queue for a given receiver
CREATE OR REPLACE FUNCTION peekQueue(queueId INTEGER, receiverId INTEGER)
RETURNS
	TABLE(sender_id INTEGER, id INTEGER, message TEXT) AS $$
DECLARE
	indices INTEGER[];
BEGIN
	indices := ARRAY[receiverId, -1];
	RETURN QUERY
		SELECT Messages.sender_id, Messages.id, Messages.message
		FROM Messages
		WHERE
			receiver_id = ANY(indices) AND queue_id = queueId
		ORDER BY Messages.timestamp ASC
		LIMIT 1;
END;
$$ LANGUAGE plpgsql;

-- Pop a topmost message for a given queue for a given receiver
CREATE OR REPLACE FUNCTION popQueue(queueId INTEGER, receiverId INTEGER)
RETURNS
	TABLE(sender_id INTEGER, id INTEGER, message TEXT) AS $$
DECLARE
	indices INTEGER[];
BEGIN
	indices := ARRAY[receiverId, -1];

	RETURN QUERY
	    DELETE FROM Messages
	    WHERE Messages.id = (SELECT Messages.id FROM Messages WHERE queue_id = queueId AND receiver_id = ANY(indices) ORDER BY Messages.timestamp ASC LIMIT 1 FOR UPDATE) RETURNING Messages.sender_id, Messages.id, Messages.message;
END;
$$ LANGUAGE plpgsql;

-- Add a given message
CREATE OR REPLACE FUNCTION addMessage(queueId INTEGER, senderId INTEGER, receiverId INTEGER, msg TEXT, OUT idx INTEGER)
AS $$
BEGIN
	INSERT INTO
		Messages (queue_id, sender_id, receiver_id, message)
	VALUES
		(queueId, senderId, receiverId, msg)
	RETURNING Messages.id
	INTO idx;
END;
$$ LANGUAGE plpgsql;

-- Check if a given message exists
CREATE OR REPLACE FUNCTION hasMessage(messageId int)
RETURNS BOOLEAN AS $$
DECLARE
	has_msg BOOLEAN := FALSE;
BEGIN
	IF EXISTS (SELECT * FROM Messages WHERE Messages.id = messageId) THEN
		has_msg := TRUE;
	END IF;

	RETURN has_msg;
END;
$$ LANGUAGE plpgsql;

-- Delete a previously found message from a queue
CREATE OR REPLACE FUNCTION deleteMessage(messageId int)
RETURNS
	VOID AS $$
BEGIN
	DELETE FROM
		Messages
	WHERE
		Messages.id = messageId;
END;
$$ LANGUAGE plpgsql;

-- Create a new queue and return index
CREATE OR REPLACE FUNCTION createQueue(creator_Id int, OUT idx INTEGER)
AS $$
BEGIN
	INSERT INTO
		Queues (creatorId)
	VALUES
		(creator_Id)
	RETURNING id
	INTO idx;
END;
$$ LANGUAGE plpgsql;

-- Check if a queue exists
CREATE OR REPLACE FUNCTION hasQueue(queue_id INTEGER)
RETURNS BOOLEAN AS $$
DECLARE
	has_queue BOOLEAN := FALSE;
BEGIN
	IF EXISTS (SELECT * FROM Queues WHERE Queues.id = queue_id) THEN
		has_queue := TRUE;
	END IF;

	RETURN has_queue;
END;
$$ LANGUAGE plpgsql;

-- Delete a queue (raise exception when there is none)
CREATE OR REPLACE FUNCTION deleteQueue(queue_id INTEGER)
RETURNS VOID AS $$
BEGIN
	IF hasQueue(queue_id) THEN
		DELETE FROM
			Queues
		WHERE
			Queues.id = queue_id;
	ELSE
		RAISE EXCEPTION 'Queue does not exist';
	END IF;
END;
$$ LANGUAGE plpgsql;

-- Poll ALL queues for messages that are destined for a given recipient (from ANY sender)
CREATE OR REPLACE FUNCTION pollQueues(receiverId INTEGER)
RETURNS 
	TABLE(queueId INTEGER) AS $$
BEGIN
	RETURN QUERY 
		SELECT DISTINCT
			Messages.queue_Id
		FROM
			Messages
		WHERE
			Messages.receiver_Id = receiverId OR Messages.receiver_id = -1;
END;
$$ LANGUAGE plpgsql;

-- Poll ALL queues for messages that are destined for a given recipient (from ANY sender)
CREATE OR REPLACE FUNCTION pollQueuesForSender(receiverId INTEGER, senderId INTEGER)
RETURNS 
	TABLE(messageId INTEGER, queueId INTEGER, messageTxt TEXT) AS $$
BEGIN
	RETURN QUERY 
		SELECT
			Messages.id, Messages.queue_id, Messages.message
		FROM
			Messages
		WHERE
			(Messages.receiver_Id = receiverId OR Messages.receiver_id = -1) AND Messages.sender_Id = senderId
		ORDER BY
			Messages.timestamp ASC
		LIMIT 1;
END;
$$ LANGUAGE plpgsql;

-- Create an account (raise exception when nickname was chosen before)
CREATE OR REPLACE FUNCTION createAccount(nickname VARCHAR(20))
RETURNS INTEGER AS $$
DECLARE
	assignedId INTEGER;
BEGIN
	IF EXISTS (SELECT * FROM Accounts WHERE Accounts.name = nickname) THEN
		RAISE EXCEPTION 'Name was chosen before!';
	ELSE
		INSERT INTO Accounts(name) VALUES (nickname) RETURNING id INTO assignedId;
	END IF;

	RETURN assignedId;
END;
$$ LANGUAGE plpgsql;

-- Delete an account (raise exception when there is none)
CREATE OR REPLACE FUNCTION deleteAccount(accountId INTEGER)
RETURNS VOID AS $$
BEGIN
	IF EXISTS (SELECT * FROM Accounts WHERE Accounts.id = accountId) THEN
		DELETE FROM Accounts WHERE Accounts.id = accountId;
	ELSE
		RAISE EXCEPTION 'User to be deleted does not exist!';
	END IF;
END;
$$ LANGUAGE plpgsql;

-- Check if an account already exists
CREATE OR REPLACE FUNCTION hasAccount(nickname VARCHAR(20))
RETURNS INTEGER AS $$
BEGIN
	IF  EXISTS (SELECT * FROM Accounts WHERE Accounts.name = nickname) THEN
		RETURN (SELECT id FROM ACCOUNTS WHERE Accounts.name = nickname);
	ELSE
		RETURN -1;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION resetTables()
RETURNS VOID AS $$
BEGIN
	-- Delete all contents
	DELETE FROM Messages;
	DELETE FROM Queues;
	DELETE FROM Accounts;

	-- Reset indices
	TRUNCATE TABLE Messages RESTART IDENTITY CASCADE;
	TRUNCATE TABLE Queues RESTART IDENTITY CASCADE;
	TRUNCATE TABLE Accounts RESTART IDENTITY CASCADE;

	-- Add global identifier
	INSERT INTO Accounts(id, name) VALUES (-1, 'Global');
END;
$$ LANGUAGE plpgsql;

-- Reference: http://forums.devshed.com/postgresql-help-21/pl-pgsql-random-data-generator-10-million-rows-668457.html
CREATE OR REPLACE FUNCTION random_string(lengh integer)
RETURNS varchar AS $$
  SELECT array_to_string(ARRAY(
          SELECT substr('abcdefghijklmnopqrstuv',trunc(random()*21+1)::int,1)
             FROM generate_series(1,$1)),'')
$$ LANGUAGE sql VOLATILE;

CREATE OR REPLACE FUNCTION fillDb(msgLen integer, nOfElements integer)
RETURNS VOID AS $$
BEGIN
    INSERT INTO Queues(id, creatorId) VALUES (-1, -1);

    INSERT INTO Messages(queue_id, sender_id, receiver_id, message)
     SELECT -1, -1, -1, random_string(msgLen) FROM generate_series(1,nOfElements);
END;
$$ LANGUAGE plpgsql;