__author__ = 'lennart'
import sys
import random
import pxssh
import subprocess
from optparse import OptionParser


def random_name(n):
    # Adapted from
    # http://stackoverflow.com/questions/2257441/random-string-generation-with-upper-case-letters-and-digits-in-python
    return ''.join(random.choice([str(i) for i in range(1, 10)]) for _ in range(n))


def main():
    # Parser for stdin
    parser = OptionParser()

    # Add some declarations
    parser.add_option('-i', '--dryad_id',
                      help='the id of the middleware dryad server',
                      metavar='ID',
                      type='int')

    parser.add_option('-p', '--dryad_port',
                      help='the port of the middleware dryad server [default: %default]',
                      metavar='PORT',
                      type='int',
                      default=1555)

    parser.add_option('-l', '--clear_logs',
                      help='whether to clear logs (middleware, db) before starting',
                      default=False,
                      action='store_true')

    parser.add_option('-r', '--reset_db',
                      help='whether to reset the database before starting',
                      default=False,
                      action='store_true')

    parser.add_option('--db_port',
                      help='database port',
                      default=5623)

    parser.add_option('-d', '--distribute_load',
                      action='store_true',
                      help='whether there should be a randomized workload distribution'
                           ' over all dryads', default=False)

    parser.add_option('-n', '--num_clients',
                      help='the number of spawned clients [default: %default]',
                      type='int',
                      default=30)

    parser.add_option('-c', '--distribute_code',
                      help='whether we should set up the client servers beforehand',
                      default=False,
                      action='store_true')

    # TODO: Check if transmitted files are necessary

    # Parse
    (options, args) = parser.parse_args()

    # Check login
    middleware_addr = 'dryad' + ('0' + str(options.dryad_id) if options.dryad_id < 10 else str(options.dryad_id)) \
                      + '.ethz.ch'
    s = pxssh.pxssh(logfile=sys.stdout.buffer)
    if not s.login(middleware_addr, 'lennartv', ''):
        print("Invalid login!")
        sys.exit(-1)

    # Reset database (if specified)
    if options.reset_db:
        print("[PY]: Resetting database...")
        s.sendline("/mnt/local/lennartv/postgres/bin/psql -h localhost -U lennartv -d lennartv -p "
                   + str(options.db_port)
                   + " -c 'SELECT resetTables()' ")
        s.prompt()

    # Clear logs (if specified)
    if options.clear_logs:
        s.prompt()
        print("[PY]: Clearing logs...")
        s.sendline('rm -r /mnt/local/lennartv/logs/*')
        s.prompt()
        s.sendline('mkdir -p /mnt/local/lennartv/logs')
        s.prompt()

    # Clear local behavior directory
    subprocess.run(["rm", "-r",  "behavior/"])
    subprocess.run(["mkdir",  "behavior/"])

    # Distribute code (if specified)
    alive_dryads = []
    if options.distribute_code:
        for i in range(1, 16 + 1):
            # Append 0 (if necessary)
            dryad_id = '0' + str(i) if i < 10 else str(i)

            if i == options.dryad_id:
                # Don't upload client's code to server
                continue

            # Wipe directories
            ssh_client = pxssh.pxssh()

            try:
                ssh_client.login('dryad' + dryad_id + '.ethz.ch', 'lennartv', '')
                ssh_client.sendline('rm -r /mnt/local/lennartv/*')
                ssh_client.prompt()
                ssh_client.logout()

                alive_dryads += [dryad_id]
            except pxssh.ExceptionPxssh:
                print("[PY]: dryad " + dryad_id + " not reachable")
                continue

            # Upload
            target = "lennartv@dryad" + dryad_id + ".ethz.ch:/mnt/local/lennartv"
            print("[PY]: Copying code to dryad " + dryad_id + "...")
            subprocess.run(args=["scp", "-r", "ant/", target])

    # Configuring clients (define inputs etc.)
    timeout = 20

    print("[PY]: Configuring clients...")
    dryad_clients = [[] for _ in range(17)]

    # Runtime

    # List of all users
    users = range(options.num_clients)

    # Every user creates exactly two queues
    queuePerUser = 1
    queues = range(1, 2 * options.num_clients)

    # Size (in bytes) of transmitted messages
    msg_size = 20 * 1024

    count = 1

    for c in range(options.num_clients):
        success = False

        # Try to establish a connection a few times
        t = 1
        while t < timeout and not success:
            # Assign random dryad
            dryad_id = random.choice(alive_dryads)
            rand_id = int(dryad_id[1]) if dryad_id[0] == '0' else int(dryad_id)

            # Check if dryad is available and login works
            try:
                ssh_conn = pxssh.pxssh()
                ssh_conn.login('dryad' + dryad_id + '.ethz.ch', 'lennartv', '')

                # Load preamble (contains explanation of instructions)
                preamble_text = ""
                with open('config/preamble', 'r') as f:
                    preamble_text = f.read()

                # Assign random name
                client_name = random_name(8)

                # Generate code
                file_name = 'code_#' + str(count) + '__' + dryad_id + '__' + client_name
                with open('behavior/' + file_name, 'w') as code:
                    # Insert preamble
                    code.write(preamble_text)

                    # TODO: Pull outside
                    #  Pre-register client (convenient for testing)
                    sql_cmd = "SELECT createAccount('" + client_name + "')"
                    ssh_cmd = "/mnt/local/lennartv/postgres/bin/psql -h localhost -p " + str(options.db_port) + " -c \""\
                              + sql_cmd + "\""

                    s.sendline(ssh_cmd)
                    s.prompt()

                    # Select a random queue ID from the previously created ones
                    queueId = count

                    # TODO: Pull outside
                    # Pre-register queue (convenient for testing)
                    queue_cmd = "SELECT createQueue(" + str(queueId) + ")"
                    ssh_queue_cmd = "/mnt/local/lennartv/postgres/bin/psql -h localhost -p " + str(options.db_port) + " -c \"" \
                                    + queue_cmd + "\""

                    s.sendline(ssh_queue_cmd)
                    s.prompt()

                    # Register client
                    code.write('CA ' + client_name + '\n')

                    # [[CODE]]
                    # for _ in range(queuePerUser):
                    #    code.write('CQ\n')

                    # We send to ourselves
                    clientId = ((count - 1) % options.num_clients) + 1

                    # Assemble instruction
                    code.write('LOOP [')

                    # Enqueue message
                    code.write('<EQ ' + str(queueId) + ' ' + str(clientId) + ' ' + str(msg_size) + ">")

                    # Wait some time..
                    code.write(',<WAIT 0>')

                    # Retrieve message from 'earlier' client
                    # code.write(',<PMFS_POP ' + str(clientId) + '>')
                    code.write(',<RQ_POP ' + str(queueId) + '>')

                    code.write(']\n')

                    # Read message from queues created by previous user

                    #  [[/CODE]]

                    # Disconnect client
                    code.write('D\n')

                # Upload code
                print("[PY]: Uploading code for '" + client_name + "' on dryad " + dryad_id + "...")

                # Create behavior directory
                ssh_conn.sendline('mkdir -p /mnt/local/lennartv/behavior')
                ssh_conn.prompt()

                subprocess.run(args=["scp", "-q",
                                     "behavior/" + file_name,
                                     "lennartv@dryad" + dryad_id + ".ethz.ch:/mnt/local/lennartv/behavior"])

                # Disconnect SSH
                ssh_conn.logout()

                # Completion
                success = True
                count += 1

                # Add to dryad to list of active dryads
                dryad_clients[rand_id] += [file_name]
            except pxssh.ExceptionPxssh:
                print("[PY]: dryad " + dryad_id + " not reachable")

            t += 1

    # Logout (middleware server)
    s.logout()

    # Fire all clients
    for k in range(1, 16 + 1):
        if len(dryad_clients[k]) == 0:
            continue

        dryad_id = '0' + str(k) if k < 10 else str(k)

        try:
            ssh_conn = pxssh.pxssh()
            ssh_conn.login('dryad' + dryad_id + '.ethz.ch', 'lennartv', '')

            for behavior_file in dryad_clients[k]:
                print("[EXEC]: " + behavior_file)
                command = 'java -jar /mnt/local/lennartv/ant/build/build_client/jar/ASL_Milestone_1_Client.jar '\
                          + middleware_addr \
                          + ' ' + str(options.dryad_port) \
                          + ' /' + 'mnt/local/lennartv/behavior/' + behavior_file

                # Avoid SIGHUP
                nohup_cmd = "nohup " + command + " > /dev/null 2> /dev/null < /dev/null &"
                print(nohup_cmd)
                ssh_conn.sendline(nohup_cmd)
                ssh_conn.prompt()

            ssh_conn.logout()

        except pxssh.ExceptionPxssh:
            print("[PY]: dryad " + dryad_id + " not reachable anymore")

if __name__ == "__main__":
    main()