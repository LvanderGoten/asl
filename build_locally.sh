#! /bin/bash
# Cleanup
rm -r ./build_server/
rm -r ./build_client/

# Compile
ant -buildfile build_server.xml jar 
ant -buildfile build_client.xml jar

