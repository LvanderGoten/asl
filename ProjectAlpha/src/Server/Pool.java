package Server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;

import Utility.ServerConfig;

public class Pool {
	private static Logger logger = LogManager.getLogger(Pool.class);
	private static Marker marker = MarkerManager.getMarker("DB_POOL");
	
	// Server config containing PostgreSQL credentials
	private ServerConfig serverConfig;
	
	// Minimum number of PostgreSQL connections
	private int numConnections;
	
	// Blocking queue
	private ArrayBlockingQueue<Connection> queue;
	
	
	public Pool (ServerConfig serverConfig) {
		this.numConnections = serverConfig.getPostgreSQLmaxConnections();
		this.serverConfig = serverConfig;
		
		// Create fair queue
		this.queue = new ArrayBlockingQueue<Connection>(this.numConnections, false);
		
		// Populate queue
		for (int i = 0; i < this.numConnections; i++) {
			Connection conn = null;
			try {
				// Check if JDBC driver is available
				Class.forName(this.serverConfig.getPostgreSQLJDBCPath());
				
				// Retrieve connection
				String uri = "jdbc:postgresql://" + this.serverConfig.getPostgreSQLServerURL() + ":" + this.serverConfig.getPostgreSQLServerPort() + "/" + this.serverConfig.getPostgreSQLDatabaseName();
				conn = DriverManager.getConnection(uri,
						this.serverConfig.getPostgreSQLUserName(),
						this.serverConfig.getPostgreSQLUserPassword());

				// Add to queue
				queue.add(conn);
				
				logger.info(Pool.marker, "[" + Integer.toString(i) + "/" + Integer.toString(this.numConnections) + "] connections");
				
			} catch (ClassNotFoundException e) {
				logger.error(Pool.marker, "PostgreSQL JDBC was not found!");
			} catch (SQLException e) {
				e.printStackTrace();
				logger.error(Pool.marker, "Check your PostgreSQL credentials for validity!");
			}
		}
		
	}
	
	public Connection next() {
		Connection conn = null;
		long acquireStart = System.nanoTime();
		try {
			conn = this.queue.take();

			//logger.info("Free connections: " + Integer.toString(this.queue.size()));
			//logger.exit();
		} catch (InterruptedException e) {
			System.out.println("Fatal interrupt in db queue!");
		}
		long acquireTime = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - acquireStart);
		//logger.info(Pool.marker, "Pool next() took " + Long.toString(acquireTime) + " ms");
		
		return conn;
	}
	
	public void add(Connection conn) {
		try {
			this.queue.put(conn);
			//logger.info("Returned db connection!");
		} catch (InterruptedException e) {
			System.out.println("Fatal interrupt in db queue!");
		}
	}
	
	public void halt() {
		while (!this.queue.isEmpty()) {
			try {
				this.queue.take().close();
			} catch (SQLException | InterruptedException e) {
				System.out.println("Fatal interrupt in db queue!");
			}
		}
	}
}
