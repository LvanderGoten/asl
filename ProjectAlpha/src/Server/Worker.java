package Server;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.*;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;

import Communication.ConnectionClose;
import Communication.Request;
import Communication.RequestVisitor;
import Communication.ServerResponse;
import Utility.Message;
import Utility.ServerConfig;

public class Worker extends Thread {
	// Logging
	private static Logger logger = LogManager.getLogger(Worker.class);
	private static Marker processingMarker = MarkerManager.getMarker("PROCESSING");
    private static Marker serviceTimeMarker = MarkerManager.getMarker("SERVICE_TIME");
    private static Marker dbWaitTimeMarker = MarkerManager.getMarker("DB_WAIT_TIME");

	// Socket
	private Socket clientSocket;
	// Are we in running state?
	private boolean running;
	// Is there a task that the thread is processing?
	private boolean busy;
	// Back reference to connection queue
	private ThreadPool connectionQueue;
	
	public Worker(ThreadPool connectionQueue) {
		this.connectionQueue = connectionQueue;

	}
	
	public boolean isRunning() {
		return this.running;
	}
	
	/**
	 * Allows the OS to set the thread's mode to idle
	 */
	public void kill() {
		this.running = false;
	}
	
	public boolean isBusy() {
		return this.busy;
	}
	
	@Override
	public void run() {

		// Set running state
		this.running = true;
		
		while (this.running) {
			// We need to retrieve a socket first
			Socket socket = this.connectionQueue.next();


            long startDbWait = System.nanoTime();
			// We also need a db connection
			Connection conn = this.connectionQueue.getSource().next();
            long dbWaitDuration = TimeUnit.NANOSECONDS.toMicros(System.nanoTime() - startDbWait);
            logger.info(Worker.dbWaitTimeMarker, Long.toString(dbWaitDuration));
			

			if (running && socket != null && conn != null) {
				RequestVisitor visitor = new RequestVisitor(conn, this.connectionQueue.getServerConfig());
				
				// Mark as being busy
				this.busy = true;

				try {
                    // Measure service time from here!!!
                    long startTime = System.nanoTime();

					// Register streams
					ObjectOutputStream output = new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream()));
					output.flush();
					ObjectInputStream input = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
					
					Object o = input.readObject();

                    // Measure service time from here.. (DEPRECATED)
                    // long startTime = System.nanoTime();

					if (o instanceof Request) {
						// Cast to appropriate data type
						Request request = (Request) o;
						
						// Delegate to visitor
						ServerResponse serverResponse = request.accept(visitor);

                        // ..to here
                        long duration = TimeUnit.NANOSECONDS.toMicros(System.nanoTime() - startTime);
                        logger.info(Worker.serviceTimeMarker, Long.toString(duration));

						// Send response to client
						output.writeObject(serverResponse);
						output.flush();

					}

					// Free resources
					input.close();
					output.close();
					
				} catch (IOException e) {
					logger.error(Worker.processingMarker, "Error while registering/deregistering I/O streams!");
				} catch (ClassNotFoundException e) {
					logger.error(Worker.processingMarker, "Sent object has an unknown type!");
				}
				
				// Job is over, mark idle
				this.busy = false;
			}

			// Free socket
			try {
				if (socket != null)
					socket.close();
			} catch (IOException e) {
				logger.error(Worker.processingMarker, "Error while closing socket!");
			}

			if (conn != null)
				this.connectionQueue.getSource().add(conn);
			

		}
	}

}
