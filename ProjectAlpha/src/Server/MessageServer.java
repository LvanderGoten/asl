package Server;
import java.io.*;
import java.net.*;

import Utility.ServerConfig;

import org.postgresql.ds.*;
import org.apache.logging.log4j.*;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import java.sql.*;

public class MessageServer implements Runnable {
	// Log4j logger
	private Logger logger;
	
	// INIT marker
	private static Marker initMarker = MarkerManager.getMarker("INIT");
	
	// Server configuration
	private ServerConfig serverConfig;
	
	// Server socket
	private ServerSocket serverSocket;
	
	// PostgreSQL connection pool
	private PGPoolingDataSource source;
	
	// PostgreSQL connection
	private Connection postgreSQLConnection;
	
	// Connection administration system
	private ThreadPool connectionQueue;
	
	// Port number to be used
	private int port;
	
	// Is the server running?
	private boolean running = false;
	
	public int getPort() {
		return port;
	}

	public boolean isRunning() {
		return this.running;
	}
	
	// Shut down the server system
	public void stopServer()
	{
		this.running = false;
		
		// Close server socket
		try {
			this.serverSocket.close();
		} catch (IOException e) {
			System.err.println("Error encountered while shutting down server!");
		}
	
		// Close DB connection
		try {
			this.postgreSQLConnection.close();
		} catch (SQLException e) {
			System.err.println("Error while closing DB connection!");
		}
		
		// Close connection management system
		this.connectionQueue.halt();
	
	}

	public MessageServer (ServerConfig serverConfig)
	{
		// Logging
        this.logger = LogManager.getLogger(MessageServer.class);
		
		this.serverConfig = serverConfig;

		// Server socket
		ServerSocket serverSocket = null;
		
		// Extract port
		int port = serverConfig.getPort();
		
		// Extract server timeout
		int acceptTimeout = serverConfig.getAcceptTimeout();
		
		this.logger.info(MessageServer.initMarker, "Creating server socket...");
		
		try {
			serverSocket = new ServerSocket(port);
			
		} catch (IOException e) {
			this.logger.error(MessageServer.initMarker, "Input/Output error occured while setting up server socket!");
		}
		catch (SecurityException e) {
			this.logger.error(MessageServer.initMarker, "Server socket set up lacks proper authorization!");
		}
		catch (IllegalArgumentException e)
		{
			this.logger.error(MessageServer.initMarker, "Port number does NOT reside in correct port range!");
		}
		
		this.logger.info(MessageServer.initMarker, "Created server socket");
		
		// Set timeout for accept
		try {
			serverSocket.setSoTimeout(acceptTimeout);
		} catch (SocketException e) {
			this.logger.error(MessageServer.initMarker, "Timeout for accept() expired!");
		}
		
		// Store configuration
		this.serverSocket = serverSocket;
		this.port = port;
		
		// Setup connection administration system
		this.connectionQueue = new ThreadPool(serverConfig.getMinThreads(), serverConfig.getMaxThreads(), serverConfig.getMaxQueueLength(), new Pool(serverConfig), serverConfig);
		
		this.logger.info(MessageServer.initMarker, "Listening to port: " + port);
		this.running = true;
	}

	@Override
	public void run() {
		// Only perform action if server is scheduled to run
		while (isRunning())
		{
			Socket clientSocket = null;
			
			try {
				clientSocket = this.serverSocket.accept();
				
				// Add to connection management system
				this.connectionQueue.add(clientSocket);
			} catch (IOException e) {
				this.logger.error(MessageServer.initMarker, "Server/client handshake failed!");
				e.printStackTrace();
			}

		}
		
		// Free resources
		this.stopServer();
	}
}
