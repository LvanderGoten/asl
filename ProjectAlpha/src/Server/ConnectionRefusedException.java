package Server;

public class ConnectionRefusedException extends Exception {
	private String err;
	
	public ConnectionRefusedException(String err) {
		this.err = err;
	}
	
	@Override
	public String toString() {
		return this.err;
	}
}
