package Server;

import java.sql.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;

import Utility.ServerConfig;

public class MessageIntermediator {
	public static final String VERSION = "1.5";
	// INIT marker
	public static Marker coreMarker = MarkerManager.getMarker("CORE");
	// Log4j logger
	private static Logger logger;
	
	// Log4j 2 configuration file
	static {
		logger = LogManager.getLogger(MessageIntermediator.class);
	}

	public static void main(String[] params) {

		ServerConfig serverConfig = new ServerConfig(params[0]);
		
		// Output some general information
		logger.info(MessageIntermediator.coreMarker, "Message Intermediator v" + MessageIntermediator.VERSION);
		
		// Start server task
		MessageServer server = new MessageServer(serverConfig);
		new Thread(server).start();
		
		// Wait some time to let clients connect
		try {
			int timeout = serverConfig.getShutdownTimeout();
			
			if (timeout != 0)
				Thread.sleep(timeout);
			else
				Thread.sleep(Long.MAX_VALUE);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		logger.info(MessageIntermediator.coreMarker, "Stopping server");
		server.stopServer();
		logger.info(MessageIntermediator.coreMarker, "Stopped server!");
		System.exit(0);
	}

}
