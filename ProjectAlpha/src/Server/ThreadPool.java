package Server;

import java.net.Socket;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;

import Communication.RequestVisitor;
import Utility.ServerConfig;

public class ThreadPool {
	private static Logger logger = LogManager.getLogger(ThreadPool.class);
	private static Marker marker = MarkerManager.getMarker("THREAD_POOL");
	
	// Server Config
	private ServerConfig serverConfig;
	
	// Minimum number of threads
	private int minThreads;
	
	// Maximum number of threads
	private int maxThreads;
	
	// Maximum number of connections within queue
	private int maxQueueLength;
	
	// Instantaneous number of threads
	private int currThreads;
	
	// Set of available threads (i.e. thread pool)
	private List<Worker> threadPool;
	
	// Encapsulated data structure
	private ArrayBlockingQueue<Socket> socketQueue;
	
	// Is the facility alive?
	private boolean alive;
	
	// Pool for PostgreSQL connections (thread-safe)
	private Pool source;
	
	public ThreadPool (int minThreads, int maxThreads, int maxQueueLength, Pool source, ServerConfig serverConfig) {
		this.minThreads = minThreads;
		this.maxThreads = maxThreads;
		this.maxQueueLength = maxQueueLength;
		this.source = source;

		this.serverConfig = serverConfig;

		this.currThreads = 0;

		this.socketQueue = new ArrayBlockingQueue<Socket>(maxQueueLength, false);

		// Spawn minimum number of threads
		this.threadPool = new ArrayList<Worker>();
		spawnThreads(maxThreads);

		this.alive = true;
	}
	
	void spawnThreads (int nOfThreads) {
		// Don't allow more threads than specified
		if (nOfThreads > this.maxThreads)
			return;

		for (int k = this.currThreads; k < nOfThreads; k++) {
			Worker thread = new Worker(this);

			// Activate thread
			thread.start();

			// Add to thread pool
			this.threadPool.add(thread);
			this.currThreads++;
		}

		logger.info(ThreadPool.marker, "There are now " + this.currThreads + " thread(s) in the system");
	}
	
	public int getQueueLength() {
		return this.socketQueue.size();
	}
	
	public Pool getSource() {
		return source;
	}

	public ServerConfig getServerConfig() {
		return this.serverConfig;
	}
	
	public synchronized void halt() {
		
		for (Worker thread : this.threadPool) {
			thread.kill();
		}
		
		this.alive = false;
		
		this.source.halt();
		
		notifyAll();
	}
	
	public void add (Socket socket) {
		this.socketQueue.add(socket);
	}
	
	public Socket next() {
		Socket socket = null;
		long timeStart = System.nanoTime();
		try {
			socket = this.socketQueue.take();
		} catch (InterruptedException e) {
			logger.info(ThreadPool.marker, "Error while popping queue!");
		}
		long duration = TimeUnit.NANOSECONDS.toNanos(System.nanoTime() - timeStart);
		//logger.info(ThreadPool.marker, "Thread next() took " + Long.toString(duration) + " ms");

		return socket;
	}
	

}
