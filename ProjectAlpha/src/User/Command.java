package User;

public class Command {
	// Kind of instruction
	private Parser.type instr;
	
	// List of arguments
	private int[] args;
	
	public Command(String line) throws ParseException {
		// Use spaces as delimiter
		String[] tokenized = line.split(" ");
		
		String instruction = tokenized[0];
		
		this.args = new int[tokenized.length - 1];
		

		if (instruction.equals("CA")) {
			instr = Parser.type.CA;
		} else if (instruction.equals("D")) {
			instr = Parser.type.D;
		} else if (instruction.equals("CQ")) {
			instr = Parser.type.CQ;
		} else if (instruction.equals("DQ")) {
			instr = Parser.type.DQ;
		} else if (instruction.equals("PMFS_POP")) {
			instr = Parser.type.PMFS_POP;
		} else if (instruction.equals("PMFS_PEEK")) {
			instr = Parser.type.PMFS_PEEK;
		} else if (instruction.equals("EQ")) {
			instr = Parser.type.EQ;
		} else if (instruction.equals("GQWM")) {
			instr = Parser.type.GQWM;
		} else if (instruction.equals("RQ_POP")) {
			instr = Parser.type.RQ_POP;
		} else if (instruction.equals("RQ_PEEK")) {
			instr = Parser.type.RQ_PEEK;
		} else if (instruction.equals("WAIT")) {
			instr = Parser.type.WAIT;
		} else if (instruction.equals("LOOP")) {
			instr = Parser.type.LOOP;
		} else {
			throw new ParseException("Invalid instruction found!");
		} 
		
		if (instr != Parser.type.LOOP) {
			// Convert to ints (we only expect IDs)
			for (int i = 1; i < tokenized.length; i++) {
				this.args[i - 1] = Integer.parseInt(tokenized[i]);
			}
		}
		
	}

	public Parser.type getInstruction() {
		return instr;
	}
	
	public int getArgument(int index) throws IndexOutOfBoundsException {
		if (index < this.args.length) {
			return this.args[index];
		} else {
			throw new IndexOutOfBoundsException();
		}
	}

	public int[] getArguments() {
		return this.args;
	}
}