package User;

public class ParseException extends Exception {
	private String msg;
	
	public ParseException(String msg) {
		this.msg = msg;
	}
	
	@Override
	public String toString() {
		return this.msg;
	}
}
