package User;

import Communication.*;
import Utility.Message;
import Utility.PostgreSQLStorageManagement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import Exception.QueueNotFoundException;
import Exception.QueueEmptyException;

/**
 * Created by lennart on 12/8/15.
 */
public class ClientDirectDb {
    // Default hostname for testing purposes
    public static final String DEFAULT_HOSTNAME = "localhost";
    public static final String DEFAULT_CHARSET = "utf-8";
    // Logging
    private static Logger logger = LogManager.getLogger(Client.class);
    public static Marker responseTimeMarker = MarkerManager.getMarker("RESPONSE_TIME");
    public static Marker setupTimeMarker = MarkerManager.getMarker("SETUP_TIME");
    public static Marker jiggleTimeMarker = MarkerManager.getMarker("JIGGLE_TIME");
    public static Marker thinkTimeMarker = MarkerManager.getMarker("THINK_TIME");
    public static Marker sendTimeMarker = MarkerManager.getMarker("SEND_TIME");
    public static Marker receiveTimeMarker = MarkerManager.getMarker("RECEIVE_TIME");

    PostgreSQLStorageManagement pSQLManagement;

    public ClientDirectDb(String[] params) {

        String serverAddr = params[0];
        String serverPort = "5432";
        File behaviorFile = new File(params[2]);

        // Check if JDBC driver is available
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        // Retrieve connection
        String uri = "jdbc:postgresql://" + serverAddr + ":" + serverPort + "/" + "other_user";
        try {
            Connection conn = DriverManager.getConnection(uri,"other_user", "");
            this.pSQLManagement = new PostgreSQLStorageManagement(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // Perform a few actions (sending, reading etc.)
        List<Request> instructions = null;
        try {
            instructions = Parser.parseBehaviorFile(behaviorFile);
        } catch (FileNotFoundException e) {
            System.err.println("Behavior file was not found!");
        } catch (ParseException e) {
            System.err.println("Behavior file is not parsable!");
        }

        System.out.println("SQL CONNECTION acquired!");

        processTaskList(instructions);
    }

    public void sendObject(Request o) {
        if (o instanceof MessagePushRequest) {
            MessagePushRequest messagePush = (MessagePushRequest) o;
            Message msg = messagePush.getMessage();
            int queueId = messagePush.getQueueTargetId();

            PostgreSQLStorageManagement.PostgreSQLQueue queue = null;
            try {
                queue = (PostgreSQLStorageManagement.PostgreSQLQueue) this.pSQLManagement.retrieveQueueByIndex(queueId);
            } catch (QueueNotFoundException e) {
                e.printStackTrace();
            }

            queue.pushMessage(msg);
        } else if (o instanceof MessagePullRequest) {
            MessagePullRequest messagePull = (MessagePullRequest) o;
            int queueId = messagePull.getQueueId();

            PostgreSQLStorageManagement.PostgreSQLQueue queue = null;
            try {
                queue = (PostgreSQLStorageManagement.PostgreSQLQueue) this.pSQLManagement.retrieveQueueByIndex(queueId);
            } catch (QueueNotFoundException e) {
                e.printStackTrace();
            }

            int issuerId = messagePull.getIssuerId();

            try {
                queue.pop(issuerId);
            } catch (QueueEmptyException e) {
                e.printStackTrace();
            }
        }
    }

    public void processTaskList(List<Request> task_list)
    {
        // Get iterator
        Iterator<Request> it = task_list.iterator();

        // AccountRequest (guaranteed to be first)
        it.next();

        long thinkTimeStart = 0L;
        long thinkTime;

        int accountId = -1;

        // Execute task list
        while (it.hasNext()) {

            // Retrieve next Request
            Request request = it.next();

            ArrayList<Request> tasks = new ArrayList<Request>();
            boolean is_loop = request instanceof Glue;

            if (is_loop){
                Glue glue = (Glue) request;

                int n = glue.getLength();

                for (int h = 0; h < n; h++) {
                    tasks.add(glue.getElement(h));
                }
            } else {
                tasks.add(request);
            }

            int round = 0;

            while (true) {
                for (Request task: tasks) {
                    if (task instanceof WaitRequest) {

                        if (((WaitRequest) task).getMillis() > 0)
                        {
                            try {
                                Thread.sleep(((WaitRequest) task).getMillis());
                                continue;
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }

                        continue;
                    }
                    else if (task instanceof MessagePushRequest) {
                        MessagePushRequest messagePush = (MessagePushRequest) task;

                        accountId = messagePush.getQueueTargetId();

                        long jiggleStart = System.nanoTime();
                        messagePush.jiggle();
                        long jiggleDuration = TimeUnit.NANOSECONDS.toMicros(System.nanoTime() - jiggleStart);
                        logger.info(ClientDirectDb.jiggleTimeMarker, Long.toString(jiggleDuration));
                    }

                    task.setIssuerId(accountId);

                    thinkTime = TimeUnit.NANOSECONDS.toMicros(System.nanoTime() - thinkTimeStart);
                    logger.info(ClientDirectDb.thinkTimeMarker, Long.toString(thinkTime));

                    long start = System.nanoTime();
                    sendObject(task);
                    long responseTime = TimeUnit.NANOSECONDS.toMicros(System.nanoTime() - start);
                    logger.info(ClientDirectDb.responseTimeMarker, Long.toString(responseTime));

                    thinkTimeStart = System.nanoTime();
                }

                if (!is_loop)
                    break;
            }
        }
    }
}
