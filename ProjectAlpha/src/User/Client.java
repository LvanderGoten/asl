package User;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;

import Communication.AccountAcknowledgement;
import Communication.ConnectionClose;
import Communication.Glue;
import Communication.MessageBucket;
import Communication.MessagePushRequest;
import Communication.Request;
import Communication.ServerResponse;
import Communication.WaitRequest;

public class Client {

	// Default hostname for testing purposes
	public static final String DEFAULT_HOSTNAME = "localhost";
	public static final String DEFAULT_CHARSET = "utf-8";
	// Logging
	private static Logger logger = LogManager.getLogger(Client.class);
	private static Marker responseTimeMarker = MarkerManager.getMarker("RESPONSE_TIME");
    private static Marker setupTimeMarker = MarkerManager.getMarker("SETUP_TIME");
    private static Marker jiggleTimeMarker = MarkerManager.getMarker("JIGGLE_TIME");
    private static Marker thinkTimeMarker = MarkerManager.getMarker("THINK_TIME");
    private static Marker sendTimeMarker = MarkerManager.getMarker("SEND_TIME");
    private static Marker receiveTimeMarker = MarkerManager.getMarker("RECEIVE_TIME");
	// Server address used by the client for bidirectional communication
	protected InetAddress serverAddr;
	protected int serverPort;
	
	protected Socket socket;
	
	private ObjectInputStream in;
	private ObjectOutputStream out;
	
	public Client (String[] params)
	{
		this.serverAddr = null;
		File behaviorFile = null;

		// Check for parameter sanity
		if (params != null)
		{
			// Check if specified hostname is valid AND reachable
			try {
				this.serverAddr = InetAddress.getByName(params[0]);
			} catch (UnknownHostException e) {
				System.err.println("You have EITHER specified an invalid host name OR the host is not reachable!");
			}

			// Check if port number is valid
			try {
				this.serverPort = Integer.parseInt(params[1]);

				// Check for correct range
				if (this.serverPort < 0 || this.serverPort > Character.MAX_VALUE)
				{
					System.err.println("Please specify a port number in the correct range!!");

					// Terminate process
					System.exit(-1);
				}
			} catch (NumberFormatException e)
			{
				System.err.println("Please specify a VALID port number!");
			}

			// Behavior file
			behaviorFile = new File(params[2]);

		}
		else {
			System.err.println("Please specify a CORRECT hostname/port combination");
			System.exit(-1);
		}

		// Assumption (due to previous code): InetAddress + port are valid specifications, check for reachability

		// Create socket
		try {
			this.socket = new Socket(this.serverAddr, this.serverPort);
		} catch (IOException e) {
			System.err.println("Specified address is NOT reachable!");
		}

		// Perform a few actions (sending, reading etc.)
		List<Request> instructions = null;
		try {
			instructions = Parser.parseBehaviorFile(behaviorFile);
		} catch (FileNotFoundException e) {
			System.err.println("Behavior file was not found!");
		} catch (ParseException e) {
			System.err.println("Behavior file is not parsable!");
		}

		processTaskList(instructions);
	}
	
	public static void main(String[] params)
	{
		// Use this to ISOLATE the database
		//new ClientDirectDb(params);
        new Client(params);
	}
	
	public void sendObject(Object o)
	{
		try {
			this.out.writeObject(o);
			this.out.flush();
		} catch (IOException e) {
			System.err.println("Error: Couldn't send object!");
		}
	}
	
	public Object readObject()
	{
		Object obj = null;
		try {
			obj = this.in.readObject();
		} catch (ClassNotFoundException e) {
			System.err.println("Only objects that inherit from Request can be transmitted!");
		} catch (IOException e) {
			System.err.println("Fatal I/O error!");
		}

		return obj;
	}
	
	public void openInput() throws IOException {
		this.in = new ObjectInputStream(new BufferedInputStream(this.socket.getInputStream()));
	}
	
	public void closeInput() throws IOException {
		this.in.close();
	}
	
	public void openOutput() throws IOException {
		this.out = new ObjectOutputStream(new BufferedOutputStream(this.socket.getOutputStream()));
		this.out.flush();
	}
	
	public void closeOutput() throws IOException {
		this.out.close();
	}
	
	public void renewSocket() {
		try {
			this.socket = new Socket(this.serverAddr, this.serverPort);
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Specified address is NOT reachable!");
		}
	}
	
	public void processTaskList(List<Request> task_list)
	{

	    try {
			// Register output stream
			openOutput();

		    // Register input stream
			openInput();

			// Get iterator
			Iterator<Request> it = task_list.iterator();

			// AccountRequest (guaranteed to be first)
			sendObject(it.next());
			AccountAcknowledgement accountAck = (AccountAcknowledgement) readObject();

			// Close streams
			closeInput();
			closeOutput();

			// Close socket
			this.socket.close();

			// Assigned account id (account possibly exists)
			int accountId = accountAck.getAccountId();

            long thinkTimeStart = 0L;
            long thinkTime;

			// Execute task list
			while (it.hasNext()) {

				// Retrieve next Request
				Request request = it.next();

				ArrayList<Request> tasks = new ArrayList<Request>();
				boolean is_loop = request instanceof Glue;

				if (is_loop){
					Glue glue = (Glue) request;

					int n = glue.getLength();

					for (int h = 0; h < n; h++) {
						tasks.add(glue.getElement(h));
					}
				} else {
					tasks.add(request);
				}

				int round = 0;

				while (true) {
					for (Request task: tasks) {
						if (task instanceof WaitRequest) {

							if (((WaitRequest) task).getMillis() > 0)
							{
								try {
									Thread.sleep(((WaitRequest) task).getMillis());
									continue;
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}

                            continue;
						}
						else if (task instanceof MessagePushRequest) {
                            long startJiggle = System.nanoTime();
							((MessagePushRequest) task).jiggle();
                            long durationJiggle = TimeUnit.NANOSECONDS.toMicros(System.nanoTime() - startJiggle);
                            logger.info(Client.jiggleTimeMarker, Long.toString(durationJiggle));
						}

                        long startOpenStreams = System.nanoTime();

                        // Eventually place thinking time stopper here..

						// Renew socket
						this.renewSocket();

                        // Stop thinking time
                        thinkTime = TimeUnit.NANOSECONDS.toMicros(System.nanoTime() - thinkTimeStart);
                        logger.info(Client.thinkTimeMarker, Long.toString(thinkTime));

						// Open streams
						openOutput();
						openInput();

                        long durationOpenStreams = TimeUnit.NANOSECONDS.toMicros(System.nanoTime() - startOpenStreams);

                        logger.info(Client.setupTimeMarker, Long.toString(durationOpenStreams));

						// Set account id (this is necessary because the account id is assigned dynamically)
						task.setIssuerId(accountId);

						// Measure time from here..
						long startTime = System.nanoTime();

						// TODO: Directly to database
						sendObject(task);

                        long sendDuration = TimeUnit.NANOSECONDS.toMicros(System.nanoTime() - startTime);
                        logger.info(Client.sendTimeMarker, Long.toString(sendDuration));

						ServerResponse serverResponse = null;

						// Retrieve ServerResponse (non-existent after ConnectionClose)
						if (!(task instanceof ConnectionClose))
						{
                            long startReceive = System.nanoTime();
							serverResponse = (ServerResponse) readObject();
                            long receiveDuration = TimeUnit.NANOSECONDS.toMicros(System.nanoTime() - startReceive);
                            logger.info(Client.receiveTimeMarker, Long.toString(receiveDuration));

                            if (serverResponse instanceof MessageBucket) {
								MessageBucket msgBucket = (MessageBucket) serverResponse;
								round++;
							}

							// ... to here
							long duration = System.nanoTime() - startTime;

							long elapsed_millis = TimeUnit.NANOSECONDS.toMicros(duration);

							// Log
							logger.info(Client.responseTimeMarker, Long.toString(elapsed_millis));
						}

                        // Measure thinking time
                        thinkTimeStart = System.nanoTime();

						// Close streams
						closeInput();
						closeOutput();

						// Close socket
						this.socket.close();
					}

					if (!is_loop)
						break;
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Problems while registering I/O streams!");
		}
	}
}
