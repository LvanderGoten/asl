package User;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import Communication.*;

public class Parser {
	public enum type {CA, D, CQ, DQ, PMFS_POP, PMFS_PEEK, EQ, GQWM, RQ_POP, RQ_PEEK, LOOP, WAIT}

	
	static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	static Random rnd = new Random();

	
	// Adapted from: http://stackoverflow.com/questions/41107/how-to-generate-a-random-alpha-numeric-string
	static String randomString( int len ){
	   StringBuilder sb = new StringBuilder( len );
	   for( int i = 0; i < len; i++ ) 
	      sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
	   return sb.toString();
	}
	
	public static List<Request> parseBehaviorFile(File file) throws ParseException, FileNotFoundException {
		// Sequeuntial list of instructions (i.e. Request objects)
		List<Request> instructions = new ArrayList<Request>();
		
		// Utility to iterate over lines
		Scanner scanner = new Scanner(file);
		
		if (!scanner.hasNextLine()) {
			scanner.close();
			throw new ParseException("Behavior file cannot be empty!");
		}
		
		// Loop over other instructions
		int line_counter = 0;
		while (scanner.hasNextLine()) {
			
			String line = scanner.nextLine();
			
			// Ignore comments
			if (line.startsWith("--"))
				continue;
			
			// Convert into Command object
			Command cmd = new Command(line);
			
			// Parsed object
			Request request = null;
			int queueId, recipientId, sourceId;
			switch (cmd.getInstruction()) {
			case CA: // CONNECT AS 
				if (line_counter == 0) {
					request = new AccountRequest(Integer.toString(cmd.getArgument(0)));
				} else {
					throw new ParseException("Connection request must be the first instruction!");
				}
				break;
			case CQ: // CREATE QUEUE
				request = new QueueRequest(QueueRequest.Action.CREATE_QUEUE);
				break;
			case D: // DISCONNECT
				request = new ConnectionClose();
				
				if (scanner.hasNextLine())
					throw new ParseException("Connection close must be the last instruction!");
				break;
			case DQ: // DELETE QUEUE
				queueId = cmd.getArgument(0);
				
				request = new QueueRequest(QueueRequest.Action.DELETE_QUEUE, queueId);
				break;
			case EQ: // ENQUEUE
				queueId = cmd.getArgument(0);
				recipientId = cmd.getArgument(1);

				String msgTxt = randomString(cmd.getArgument(2));
				
				request = new MessagePushRequest(recipientId, queueId, msgTxt);
				break;
			case GQWM: // GET QUEUES WITH MESSAGES
				
				request = new QueueRequest(QueueRequest.Action.POLL_QUEUES);
				break;
			case PMFS_PEEK: // PULL MESSAGES FROM SENDER (PEEK)
				sourceId = cmd.getArgument(0);
				
				request = new MessagePullRequest(sourceId, -1, true);
				break;
			case PMFS_POP: // PULL MESSAGES FROM SENDER (POP)
				sourceId = cmd.getArgument(0);
				
				request = new MessagePullRequest(sourceId, -1, false);
				break;
			case RQ_PEEK: // READ QUEUE (PEEK)
				queueId = cmd.getArgument(0);
				
				request = new MessagePullRequest(queueId, true);
				
				break;
			case RQ_POP: // READ QUEUE (POP)
				queueId = cmd.getArgument(0);
				
				request = new MessagePullRequest(queueId, false);
				break;
			case WAIT:
				int time = cmd.getArgument(0);
				
				request = new WaitRequest(time);
				break;
			case LOOP:
				ArrayList<Request> collect = new ArrayList<Request>();
				
				int pos = line.indexOf('<', 0);
				while (pos != -1) {
					int end_pos = line.indexOf('>', pos + 1);
					
					if (end_pos == -1)
						throw new ParseException("Bracket doesn't close in loop statement!");
					
					Command in_cmd = new Command(line.substring(pos + 1, end_pos));
					
					Request in_request = null;
					int in_queueId, in_recipientId, in_sourceId;
					switch (in_cmd.getInstruction()) {
						case CQ: // CREATE QUEUE
							in_request = new QueueRequest(QueueRequest.Action.CREATE_QUEUE);
							break;
						case DQ: // DELETE QUEUE
							in_queueId = in_cmd.getArgument(0);
							
							in_request = new QueueRequest(QueueRequest.Action.DELETE_QUEUE, in_queueId);
							break;
						case EQ: // ENQUEUE
							in_queueId = in_cmd.getArgument(0);
							in_recipientId = in_cmd.getArgument(1);
							

							String in_msgTxt = randomString(in_cmd.getArgument(2));
							
							in_request = new MessagePushRequest(in_recipientId, in_queueId, in_msgTxt);
							break;
						case GQWM: // GET QUEUES WITH MESSAGES
							
							in_request = new QueueRequest(QueueRequest.Action.POLL_QUEUES);
							break;
						case PMFS_PEEK: // PULL MESSAGES FROM SENDER (PEEK)
							in_sourceId = in_cmd.getArgument(0);
							
							in_request = new MessagePullRequest(in_sourceId, -1, true);
							break;
						case PMFS_POP: // PULL MESSAGES FROM SENDER (POP)
							in_sourceId = in_cmd.getArgument(0);
							
							in_request = new MessagePullRequest(in_sourceId, -1, false);
							break;
						case RQ_PEEK: // READ QUEUE (PEEK)
							in_queueId = in_cmd.getArgument(0);
							
							in_request = new MessagePullRequest(in_queueId, true);
							
							break;
						case RQ_POP: // READ QUEUE (POP)
							in_queueId = in_cmd.getArgument(0);
							
							in_request = new MessagePullRequest(in_queueId, false);
							break;			
						case WAIT:
							int in_time = in_cmd.getArgument(0);
							
							in_request = new WaitRequest(in_time);
							break;
						default:
							throw new ParseException("Unknown instruction type encountered!");
					}
					
					collect.add(in_request);
					
					
					pos = line.indexOf('<', end_pos);
				}
				
				request = new Glue(collect);
				
				break;
			default:
				throw new ParseException("Unknown instruction type encountered!");
			
			}
			
			instructions.add(request);
			line_counter++;
		}
		
		// Close scanner
		scanner.close();
		
		return instructions;
	}
}
