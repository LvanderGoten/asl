package Communication;

public class WaitRequest extends Request {
	
	private int millis;
	
	public WaitRequest (int millis) {
		this.millis = millis;
	}

	public int getMillis() {
		return millis;
	}

	@Override
	public ServerResponse accept(RequestVisitor visitor) {
		return null;
	}

}
