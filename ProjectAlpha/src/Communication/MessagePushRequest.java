package Communication;

import java.util.Random;

import Utility.Account;
import Utility.Message;
import Utility.StorageManagement;

public class MessagePushRequest extends Request {

	
	static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	static Random rnd = new Random();

	
	// Adapted from: http://stackoverflow.com/questions/41107/how-to-generate-a-random-alpha-numeric-string
	static String randomString( int len ){
	   StringBuilder sb = new StringBuilder( len );
	   for( int i = 0; i < len; i++ ) 
	      sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
	   return sb.toString();
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = -8658149666740903599L;

	// The encapsulated message
	private Message msg;
	
	// The targeted queue (as qualified by id)
	private int queueTargetId;
	
	/**
	 * @param issuerId The entity that created the message
	 * @param receiverId The entity that should receive the message (0 means all entities can receive it)
	 * @param queueTargetId The queue where the message should be stored
	 * @param msg The actual message
	 */
	public MessagePushRequest(int receiverId, int queueTargetId, String msg)
	{
		this.msg = new Message(queueTargetId, receiverId, msg);
		this.queueTargetId = queueTargetId;
	}
	
	public void jiggle() {
		this.msg.setText(randomString(msg.getMsg().length()));
	}
	
	public Message getMessage()
	{
		return msg;
	}
	
	public int getQueueTargetId()
	{
		return this.queueTargetId;
	}
	
	@Override
	public void setIssuerId(int issuerId) {
		super.setIssuerId(issuerId);
		
		// Propagate
		msg.setSenderId(issuerId);
	}

	@Override
	public ServerResponse accept(RequestVisitor visitor) {
		return visitor.visit(this);
	}

}
