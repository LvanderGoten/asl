package Communication;

import Utility.Message;

public class MessageBucket extends Acknowledgement {
	private Message msg;

	public MessageBucket(State state) {
		super(state);
		
		this.msg = null;
	}

	public MessageBucket(State state, Message msg) {
		super(state);
		
		this.msg = msg;
	}

	public Message getMsg() {
		return msg;
	}
}
