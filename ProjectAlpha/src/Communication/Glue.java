package Communication;

import java.util.List;

public class Glue extends Request {
	private List<Request> cmds;
	
	public Glue (List<Request> cmds) {
		this.cmds = cmds;
	}

	public int getLength() {
		return this.cmds.size();
	}
	
	public Request getElement(int index) {
		return this.cmds.get(index);
	}

	@Override
	public ServerResponse accept(RequestVisitor visitor) {
		return null;
	}

}
