package Communication;

import java.io.Serializable;

public class ConnectionClose extends Request {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2502652934692442219L;

	@Override
	public ServerResponse accept(RequestVisitor visitor) {
		return visitor.visit(this);
	}

}
