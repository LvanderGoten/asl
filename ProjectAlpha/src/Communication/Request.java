package Communication;

import java.io.Serializable;

public abstract class Request implements Serializable {
	public abstract ServerResponse accept (RequestVisitor visitor);
	
	// The client's authentication object
	protected int issuerId;
	
	public void setIssuerId(int issuerId) {
		this.issuerId = issuerId;
	}

	public int getIssuerId() {
		return issuerId;
	}
}
