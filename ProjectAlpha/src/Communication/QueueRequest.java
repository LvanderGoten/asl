package Communication;

public class QueueRequest extends Request {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7610059358392281099L;
	private Action action;
	
	public enum Action {CREATE_QUEUE, DELETE_QUEUE, POLL_QUEUES}
	
	private int queueId;
	
	// Automatic assignment of IDs (so far)
	public QueueRequest (Action action)
	{
		this(action, -1);
	}
	
	public QueueRequest (Action action, int queueId)
	{
		this.action = action;
		this.queueId = queueId;
	}
	
	public int getQueueId()
	{
		return this.queueId;
	}
	
	public Action getAction()
	{
		return this.action;
	}

	@Override
	public ServerResponse accept(RequestVisitor visitor) {
		return visitor.visit(this);
	}

}
