package Communication;

public class MessagePullRequest extends Request {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6935402338655980346L;
	
	private int sourceId;
	
	private int queueId;
	
	private boolean peek;

	public boolean isPeek() {
		return peek;
	}

	public int getSourceId() {
		return sourceId;
	}

	public int getQueueId() {
		return queueId;
	}

	// TODO: Risky
	public MessagePullRequest(int sourceId, int queueId, boolean peek) {
		
		this.sourceId = sourceId;
		this.queueId = queueId;
		this.peek = peek;
	}
	
	// TODO: Risky
	public MessagePullRequest(int queueId, boolean peek) {
		this(-1, queueId, peek);
	}

	@Override
	public ServerResponse accept(RequestVisitor visitor) {
		return visitor.visit(this);
	}

}
