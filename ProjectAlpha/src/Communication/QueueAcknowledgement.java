package Communication;

public class QueueAcknowledgement extends Acknowledgement {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2705647104339091879L;
	private Type type;
	private int queueId;
	
	// An optional payload where data can be transmitted
	private Object payload = null;
	
	public Object getPayload() {
		return payload;
	}

	public enum Type {QUEUE_CREATED, QUEUE_DELETED, QUEUE_NON_EXISTENT, QUEUE_EMPTY, QUEUE_MESSAGE_STORED, QUEUE_POLL}

	public QueueAcknowledgement(State state, Type type, int queueId) {
		super(state);
		
		this.type = type;
		this.queueId = queueId;
	}
	
	public QueueAcknowledgement(State state, Type type, Object payload) {
		super(state);
		
		this.type = type;
		this.payload = payload;
	}
	
	public Type getType()
	{
		return this.type;
	}
	
	public int getQueueId()
	{
		return this.queueId;
	}

}
