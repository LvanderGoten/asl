package Communication;

public class AccountAcknowledgement extends Acknowledgement {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4449168344835922027L;
	private Type type;
	private int accountId;
	
	public enum Type {ACCOUNT_CREATED, ACCOUNT_EXISTS}

	protected AccountAcknowledgement(State state, Type type, int accountId) {
		super(state);

		this.type = type;
		this.accountId = accountId;
	}
	
	public Type getType()
	{
		return this.type;
	}
	
	public int getAccountId()
	{
		return this.accountId;
	}

}
