package Communication;

import java.sql.Connection;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;

import Exception.QueueEmptyException;
import Exception.QueueNotFoundException;
import Utility.Account;
import Utility.Message;
import Utility.PostgreSQLStorageManagement;
import Utility.ServerConfig;
import Utility.StorageManagement;

import org.apache.logging.log4j.*;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;

public class RequestVisitor {
	// Logging
	private static Logger logger = LogManager.getLogger(RequestVisitor.class);
	private static Marker accountRegMarker = MarkerManager.getMarker("REG");
	private static Marker createQueueMarker = MarkerManager.getMarker("QUEUE_CREATED");
	private static Marker deleteQueueMarker = MarkerManager.getMarker("QUEUE_DELETED");
	private static Marker pollQueueMarker = MarkerManager.getMarker("POLL");
	private static Marker queueEmptyMarker = MarkerManager.getMarker("QUEUE_EMPTY");
	private static Marker messagePushMarker = MarkerManager.getMarker("PUSH");
	private static Marker queueInexistentMarker = MarkerManager.getMarker("QUEUE_INEXISTENT");
	private static Marker messagePullBySenderMarker = MarkerManager.getMarker("PULL_BY_SENDER");
	private static Marker messagePullByQueueMarker = MarkerManager.getMarker("PULL_BY_QUEUE");
	
	private ServerConfig serverConfig;
	private int test_mode_message_size = -1;
	
	StorageManagement storageManager;
	
	private String getZeroString(int length) {
		  if (length > 0) {
		    char[] array = new char[length];
		    Arrays.fill(array, '0');
		    return new String(array);
		  }
		  return "";
		}
	
	public static String abbreviate(String original) {
		if (original.length() < 20) {
			return original;
		} else {
			return original.substring(0, 20) + " (" + original.length() + " bytes)";
		}
	}

	public RequestVisitor(Connection postgreSQLConnection, ServerConfig serverConfig)
	{
		this.storageManager = new PostgreSQLStorageManagement(postgreSQLConnection);
		this.serverConfig = serverConfig;
	}
	
	/**
	 * @param req The AccountRequest as issued by a client to register a new account
	 */
	ServerResponse visit(AccountRequest req) {
		
		// Extract name
		String issuer_name = req.getIssuerName();
		
		// Register account
		Account issuer = storageManager.addAccount(issuer_name);
		
		// Log
		logger.info(RequestVisitor.accountRegMarker, "<Name: '" + issuer_name + "', Id: " + issuer.getAccountId() + ">");
		
		// Send account acknowledgement back to user
		return new AccountAcknowledgement(Acknowledgement.State.ACTION_SUCCEEDED, AccountAcknowledgement.Type.ACCOUNT_CREATED, issuer.getAccountId());
	}
	
	/**
	 * @param req The MessagePullRequest as issued by a client to retrieve a message for ONE queue
	 */
	ServerResponse visit(MessagePullRequest req) {
		if (this.serverConfig.isTestMode())
		{
			Message mockMsg = new Message(req.getIssuerId(), req.getIssuerId(), this.getZeroString(this.test_mode_message_size));
			
			return new MessageBucket(Acknowledgement.State.ACTION_SUCCEEDED, mockMsg);
		}
		
		// Extract encapsulate data
		int issuerId = req.getIssuerId(), sourceId = req.getSourceId(), queueId = req.getQueueId();
		String mode = req.isPeek() ? "peek" : "pop";
		try {
			StorageManagement.Queue queue = null;
			Message msg = null;
			
			if (queueId != -1) {
				// Retrieve queue
				queue = this.storageManager.retrieveQueueByIndex(queueId);
				
				// Search for element
				msg = req.isPeek() ? queue.peek(issuerId) : queue.pop(issuerId);
				
				if (msg == null)
				{
					return new QueueAcknowledgement(Acknowledgement.State.ACTION_FAILED, QueueAcknowledgement.Type.QUEUE_NON_EXISTENT, queueId);
				}

				logger.info(RequestVisitor.messagePullByQueueMarker, "<Issuer-Id: " + issuerId + ", Queue-Id: " + queue.getId() + ", Msg-Id: " + msg.getMsgId() + ", Text: '" + abbreviate(msg.getMsg()) + "', Mode: " + mode + ">" );
			}
			else
			{
				// Poll queues for specific sender
				msg = req.isPeek() ? this.storageManager.peekMessageFromSender(issuerId, sourceId) : this.storageManager.popMessageFromSender(issuerId, sourceId);
				
				if (msg == null)
				{
					return new QueueAcknowledgement(Acknowledgement.State.ACTION_FAILED, QueueAcknowledgement.Type.QUEUE_NON_EXISTENT, queueId);
				}
				
				// Get queue index that held message
				queueId = msg.getQueueId();
				
				logger.info(RequestVisitor.messagePullBySenderMarker, "<Issuer-Id: " + issuerId + ", Source-Id: " + sourceId + ", Queue-Id: " + queueId + ", Msg-Id: " + msg.getMsgId() + ", Text: '" + abbreviate(msg.getMsg()) + "', Mode: " + mode + ">" );
			}

			
			// Wrap message into new container
			MessageBucket bucket = new MessageBucket(Acknowledgement.State.ACTION_SUCCEEDED, msg);
			
			return bucket;
		} catch (QueueNotFoundException e) {
			logger.error(RequestVisitor.queueInexistentMarker, "<Queue-Id: " + queueId + ">");
			
			return new QueueAcknowledgement(Acknowledgement.State.ACTION_FAILED, QueueAcknowledgement.Type.QUEUE_NON_EXISTENT, queueId);
		} catch (QueueEmptyException e) {
			logger.error(RequestVisitor.queueEmptyMarker, "<Issuer-Id: " + issuerId + ", Queue-Id: " + (queueId != -1 ? queueId : "GLOBAL") + ">");
			
			return new QueueAcknowledgement(Acknowledgement.State.ACTION_FAILED, QueueAcknowledgement.Type.QUEUE_EMPTY, queueId);
		}
	}
	
	/**
	 * @param req The MessagePullRequest as issued by a client to submit a message
	 */
	ServerResponse visit(MessagePushRequest req) {
		if (this.serverConfig.isTestMode())
		{
			this.test_mode_message_size = req.getMessage().getMsg().length();
			return new QueueAcknowledgement(Acknowledgement.State.ACTION_SUCCEEDED, QueueAcknowledgement.Type.QUEUE_MESSAGE_STORED, req.getQueueTargetId());
		}
		
		// Extract message
		Message msg = req.getMessage();
		
		// Update timestamp
		msg.updateTimestamp();
		
		// Targeted queue
		int targetQueueIndex = req.getQueueTargetId();
		
		StorageManagement.Queue queue = null;
		// Check if queue exists
		try {
			queue = this.storageManager.retrieveQueueByIndex(targetQueueIndex);
		} catch (QueueNotFoundException e) {
			logger.error(RequestVisitor.queueInexistentMarker, "<Queue-Id: " + targetQueueIndex + ">");
			
			return new QueueAcknowledgement(Acknowledgement.State.ACTION_FAILED, QueueAcknowledgement.Type.QUEUE_NON_EXISTENT, targetQueueIndex);
		}
		
		// Store message in queue
		queue.pushMessage(msg);
		
		// Print
		logger.info(RequestVisitor.messagePushMarker, "<Sender-Id: " + req.getIssuerId() + ", Receiver-Id: " + msg.getReceiverId() + ", Queue-Id: " + queue.getId() + ", Text: '" + abbreviate(msg.getMsg()) + "'>");
		
		// Send acknowledgement back to user
		return new QueueAcknowledgement(Acknowledgement.State.ACTION_SUCCEEDED, QueueAcknowledgement.Type.QUEUE_MESSAGE_STORED, targetQueueIndex);
	}
	
	/**
	 * @param req The QueueRequest as issued by a client to either create or delete a queue
	 */
	ServerResponse visit(QueueRequest req) {
		if (serverConfig.isTestMode())
			return null;
		
		switch (req.getAction())
		{
		case CREATE_QUEUE:
			// Add to system
			StorageManagement.Queue added = storageManager.addQueue(req.getIssuerId());
			
			// Print
			logger.info(RequestVisitor.createQueueMarker, "<Creator-Id: " + req.getIssuerId() + ", Queue-Id: " + added.getId() + ">");
			
			return new QueueAcknowledgement(Acknowledgement.State.ACTION_SUCCEEDED, QueueAcknowledgement.Type.QUEUE_CREATED, added.getId());
		case DELETE_QUEUE:
			// Remove from system
			boolean success = false;
			try {
				success = storageManager.removeQueue(storageManager.retrieveQueueByIndex(req.getQueueId()));
			} catch (QueueNotFoundException e) {
				System.err.println("<DELETED_QUEUE>: #" + req.getQueueId() + " FAILED (queue doesn't exist)");
			}
			
			// Print
			logger.info(RequestVisitor.deleteQueueMarker, "<Issuer-Id: " + req.getIssuerId() + ", Queue-Id: " + req.getQueueId() + ">");
			
			return new QueueAcknowledgement(success ? Acknowledgement.State.ACTION_SUCCEEDED : Acknowledgement.State.ACTION_FAILED, QueueAcknowledgement.Type.QUEUE_DELETED, req.getQueueId());
		case POLL_QUEUES:
			int issuerId = req.getIssuerId();
			
			// Get queue indices that have messages for issuer
			List<Integer> queueIndices = this.storageManager.retrieveQueueIndicesWithMessages(issuerId);
			
			// Log
			String str = "";
			for (int i = 0; i < queueIndices.size(); i++) {
				if (i == queueIndices.size() - 1) {
					str = str + queueIndices.get(i);
				} else {
					str = str + queueIndices.get(i) + ", ";
				}
			}

			logger.info(RequestVisitor.pollQueueMarker, "<Issuer-Id: " + issuerId + ", Queue-Ids: {" + str + "}>");
			
			return new QueueAcknowledgement(Acknowledgement.State.ACTION_SUCCEEDED, QueueAcknowledgement.Type.QUEUE_POLL, queueIndices);
		default:
			return null;
		
		}
	}
	
	// TODO: Think over
	ServerResponse visit(ConnectionClose req) {
		return null;
	}
}
