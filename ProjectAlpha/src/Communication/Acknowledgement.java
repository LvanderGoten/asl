package Communication;

public abstract class Acknowledgement implements ServerResponse {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8055129860358650723L;
	private State state;

	public enum State {ACTION_SUCCEEDED, ACTION_FAILED};
	
	protected Acknowledgement (State state)
	{
		this.state = state;
	}
	
	public State getState()
	{
		return this.state;
	}
}
