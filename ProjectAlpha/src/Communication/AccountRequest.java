package Communication;

public class AccountRequest extends Request {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6942307268092965400L;
	private String name;
	
	public AccountRequest(String name)
	{
		this.name = name;
	}

	public String getIssuerName() {
		return name;
	}

	@Override
	public ServerResponse accept(RequestVisitor visitor) {
		return visitor.visit(this);
	}

}
