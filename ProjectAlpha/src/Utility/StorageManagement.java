package Utility;

import java.io.Serializable;
import java.util.List;

import Exception.QueueEmptyException;
import Exception.QueueNotFoundException;

public abstract class StorageManagement {
	
	// Add and register a new queue to the system
	public abstract Queue addQueue(int accountId);
	
	// Remove a queue
	public abstract boolean removeQueue(Queue queue);
	
	public abstract Account addAccount(String issuerName);
	
	/**
	 * @param id: The unique identifier representing the queue
	 * @return The desired queue if available 
	 * @throws QueueNotFoundException if and only if there is no queue associated to the given id
	 */
	public abstract Queue retrieveQueueByIndex(int id) throws QueueNotFoundException;
	
	public abstract List<Integer> retrieveQueueIndicesWithMessages(int issuerId);
	
	public abstract Message peekMessageFromSender(int issuerId, int sourceId) throws QueueEmptyException;
	
	public abstract Message popMessageFromSender(int issuerId, int sourceId) throws QueueEmptyException;
	
	public abstract class Queue {
		// The identifier associated to the queue
		private int id;
		
		protected Queue (int id) {
			this.id = id;
		}

		public int getId()
		{
			return this.id;
		}
		
		// Retrieve top element
		public abstract Message peek(int issuerId) throws QueueEmptyException;
		
		// Retrieve top element and advance queue (i.e. delete topmost element)
		public abstract Message pop(int issuerId) throws QueueEmptyException;
		
		public abstract void pushMessage(Message msg);
	}
}
