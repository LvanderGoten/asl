package Utility;

import java.io.Serializable;

import Utility.Account;

public class Message implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6852608087017356139L;

	// The identifier uniquely defining the message
	private int msgId;
	
	// The account uniquely defining the sender
	private int senderId;
	
	public void setSenderId(int senderId) {
		this.senderId = senderId;
	}

	// The identifier uniquely defining the receiver
	private int receiverId;
	
	// The identifier uniquely defining the queue
	private int queueId;
	
	// The timestamp relating to the arrival in the queue system
	private long timestamp;
	
	// The actual message
	private String msg;
	
	public String getMsg() {
		return msg;
	}

	public void setMsgId(int msgId) {
		this.msgId = msgId;
	}

	public int getMsgId() {
		return msgId;
	}

	public int getSenderId() {
		return this.senderId;
	}

	public int getReceiverId() {
		return receiverId;
	}

	public int getQueueId() {
		return queueId;
	}

	public long getTimestamp() {
		return timestamp;
	}
	
	public void setText(String txt) {
		this.msg = txt;
	}
	
	// Update timestamp to match current timestamp
	public void updateTimestamp() {
		timestamp = System.currentTimeMillis();
	}
	
	@Override
	public String toString() {
		return msg;
	}

	/**
	 * @param msgId The identifier uniquely defining the message
	 * @param senderId The identifier uniquely defining the sender
	 * @param receiverId The identifier uniquely defining the receiver
	 * @param msg The actual message
	 */
	public Message (int queueId, int receiverId, String msg)
	{
		this.msgId = -1; // Server automatically assigns an Id
		this.queueId = queueId;
		this.receiverId = receiverId;
		this.msg = msg;
	}
}
