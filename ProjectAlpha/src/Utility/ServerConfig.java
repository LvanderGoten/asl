package Utility;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import java.io.File;
import java.io.IOException;

public class ServerConfig {
	// Server port
	private int port;
	
	// Minimum number of threads
	private int maxThreads;

	// Maximum number of threads
	private int minThreads;
	
	// Test mode
	private boolean testMode;
	
	// Maximum number of elements within queue
	private int maxQueueLength;
	
	// Timeout until server shutdown
	private int shutdownTimeout;
	
	// Timeout until accept fails
	private int acceptTimeout;
	
	// PostgreSQL JDBC driver location
	private String postgreSQLJDBCPath;
	
	// PostgreSQL server URL
	private String postgreSQLServerURL;
	
	// PostgreSQL server port
	private int postgreSQLServerPort;
	
	// PostgreSQL user name
	private String postgreSQLUserName;
	
	// PostgreSQL user password
	private String postgreSQLUserPassword;
	
	// PostgreSQL database name
	private String postgreSQLDatabaseName;
	
	// PostgreSQL minimum connections
	private int postgreSQLminConnections;
	
	// PostgreSQL maximum connections
	private int postgreSQLmaxConnections;
	
	// Where do we place the log files
	private String logDir;


	/**
	 * Convert a XML file to a config object
	 * @param xmlCfgPath The path to the configuration XML file
	 */
	public ServerConfig (String xmlCfgPath) {
		try {
		// Create a file handle
		File xmlFile = new File(xmlCfgPath);
		
		// Get builder factory
		DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
		
		// Get document builder
		DocumentBuilder dBuilder = dFactory.newDocumentBuilder();
		
		// Get document
		Document doc = dBuilder.parse(xmlFile);
		
		// Normalize document
		doc.normalize();
		
		// Get server attributes
		NodeList serverList = doc.getElementsByTagName("server");
		Node serverNode = serverList.item(0);
		Element serverElement = (Element) serverNode;
		
		// Extract server port
		this.port = Integer.valueOf(serverElement.getAttribute("port"));
		
		// Extract shutdown timeout
		this.shutdownTimeout = Integer.valueOf(serverElement.getAttribute("shutdownTimeout"));
		
		// Extract accept timeout
		this.acceptTimeout = Integer.valueOf(serverElement.getAttribute("acceptTimeout"));
		
		// Extract minimum number of threads
		this.minThreads = Integer.valueOf(serverElement.getAttribute("minThreads"));
		
		// Extract maximum number of threads
		this.maxThreads = Integer.valueOf(serverElement.getAttribute("maxThreads"));
		
		// Extract minimum number of elements within queue
		this.maxQueueLength = Integer.valueOf(serverElement.getAttribute("maxQueueLength"));
		
		// Get environment attributes
		NodeList envList = doc.getElementsByTagName("environment");
		Node envNode = envList.item(0);
		Element envElement = (Element) envNode;
		
		// Extract object mode preference
		this.testMode = Integer.valueOf(envElement.getAttribute("test_mode")) == 1;
		
		// Extract log dir path
		this.logDir = envElement.getAttribute("logDir");
		
		// Get path attributes
		NodeList pathsList = doc.getElementsByTagName("paths");
		Node pathsNode = pathsList.item(0);
		Element pathsElement = (Element) pathsNode;
		
		// Extract PostgreSQL JDBC driver path
		this.postgreSQLJDBCPath = pathsElement.getAttribute("PostgreSQLJDBCDriver");
		
		// Get database attributes
		NodeList dbList = doc.getElementsByTagName("database");
		Node dbNode = dbList.item(0);
		Element dbElement = (Element) dbNode;
		
		// Extract PostgreSQL server URL
		this.postgreSQLServerURL = dbElement.getAttribute("url");
		
		// Extract PostgreSQL server port
		this.postgreSQLServerPort = Integer.valueOf(dbElement.getAttribute("port"));
		
		// Extract PostgreSQL user name
		this.postgreSQLUserName = dbElement.getAttribute("user");
		
		// Extract PostgreSQL user password
		this.postgreSQLUserPassword = dbElement.getAttribute("password");
		
		// Extract PostgreSQL user password
		this.postgreSQLDatabaseName = dbElement.getAttribute("name");
		
		// Extract PostgreSQL amount of minimum connections
		this.postgreSQLminConnections = Integer.parseInt(dbElement.getAttribute("minConnections"));
		
		// Extract PostgreSQL amount of maximum connections
		this.postgreSQLmaxConnections = Integer.parseInt(dbElement.getAttribute("maxConnections"));
		
		// Integrity checks
		boolean valid = true;
		
		if (this.port < 0 || this.port > Character.MAX_VALUE) {
			valid = false; 
			
			System.err.println("Please specify a valid server port!");
		}
		
		if (this.postgreSQLServerPort < 0 || this.postgreSQLServerPort > Character.MAX_VALUE) {
			valid = false;
			
			System.err.println("Please specify a valid PostgreSQL database port!");
		}
		
		// Shutdown if malformed configuration is detected
		if (!valid)
			System.exit(0);
			
		
		} catch (ParserConfigurationException e) {
			System.err.println("Fatal parser configuration error!");
		} catch (SAXException | IOException e) {
			System.err.println("Fatal parser error while parsing!");
		} 
	}

	public String getPostgreSQLDatabaseName() {
		return postgreSQLDatabaseName;
	}

	public int getPort() {
		return port;
	}
	
	public int getShutdownTimeout() {
		return shutdownTimeout;
	}

	public int getAcceptTimeout() {
		return acceptTimeout;
	}

	public String getPostgreSQLJDBCPath() {
		return postgreSQLJDBCPath;
	}

	public String getPostgreSQLServerURL() {
		return postgreSQLServerURL;
	}

	public int getPostgreSQLServerPort() {
		return postgreSQLServerPort;
	}

	public int getPostgreSQLminConnections() {
		return postgreSQLminConnections;
	}

	public String getPostgreSQLUserName() {
		return postgreSQLUserName;
	}

	public String getPostgreSQLUserPassword() {
		return postgreSQLUserPassword;
	}

	public boolean isTestMode() {
		return testMode;
	}
	
	public String getLogDir() {
		return logDir;
	}

	public int getMaxThreads() {
		return maxThreads;
	}

	public int getMinThreads() {
		return minThreads;
	}

	public int getMaxQueueLength() {
		return maxQueueLength;
	}

	public int getPostgreSQLmaxConnections() {
		return postgreSQLmaxConnections;
	}
}
