package Utility;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;

import Exception.QueueEmptyException;
import Exception.QueueNotFoundException;
import Server.Pool;

/**
 * Implementation of a  message queue system with PostgreSQL
 * @author lennart
 */
public class PostgreSQLStorageManagement extends StorageManagement {
	private static Logger logger = LogManager.getLogger(PostgreSQLStorageManagement.class);
	private static Marker marker = MarkerManager.getMarker("DB");
	private static Marker dbTimeMarker = MarkerManager.getMarker("DB_RESPONSE_TIME");

	// PostgreSQL DB connection
	private Connection postgreSQLConnection;
	
	public Connection getPostgreSQLConnection() {
		return postgreSQLConnection;
	}
	
	// Don't let others instantiate objects
	public PostgreSQLStorageManagement(Connection postgreSQLConnection) {
		this.postgreSQLConnection = postgreSQLConnection;
	}
	
	@Override
	public Queue addQueue(int accountId) {
		try {
			CallableStatement statement = getPostgreSQLConnection().prepareCall(" { call createQueue(?) } ");

			statement.setInt(1, accountId);
			ResultSet resultSet = statement.executeQuery();
			resultSet.next();
			
			// Get queue object
			int queueId = resultSet.getInt(1);
			PostgreSQLQueue queue = new PostgreSQLQueue(queueId);
			
			return queue;
		} catch (SQLException e) {
			logger.error(PostgreSQLStorageManagement.marker, "Fatal SQL error: Couldn't create queue!");
		}
		
		return null;
	}

	@Override
	public boolean removeQueue(Queue queue) {
		// Check if queue exists
		try {

			// We have to delete the queue otherwise
			CallableStatement deleteQ = getPostgreSQLConnection().prepareCall(" { call deleteQueue(?) } ");
			
			// Set input
			deleteQ.setInt(1,  queue.getId());
			
			deleteQ.execute();
			
		} catch (SQLException e) {
			logger.error(PostgreSQLStorageManagement.marker, "Fatal SQL error: Couldn't remove queue!");
		}

		return false;
	}

	@Override
	public Queue retrieveQueueByIndex(int id) throws QueueNotFoundException {
		// Check if queue exists
		try {
			CallableStatement statement = getPostgreSQLConnection().prepareCall("{ call hasQueue(?) }");

			// Set input
			statement.setInt(1, id);
			ResultSet resultSet = statement.executeQuery();
			resultSet.next();
			boolean hasQueue = resultSet.getBoolean(1);

			if (!hasQueue)
				throw new QueueNotFoundException();

			return new PostgreSQLStorageManagement.PostgreSQLQueue(id);


		} catch (SQLException e) {
			logger.error(PostgreSQLStorageManagement.marker, "Fatal SQL error: Queue does not exist!");
		}

		return null;
	}

//    @Override
//    public Queue retrieveQueueByIndex(int id) throws QueueNotFoundException {
//        return new PostgreSQLStorageManagement.DummyQueue(id);
//    }

    // Dummy queue: Use THIS to isolate the middleware
    public class DummyQueue extends Queue {
        // 200 characters
        public static final String dummyText = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu";

        protected DummyQueue (int id) {
            super(id);
        }

        @Override
        public Message peek(int issuerId) throws QueueEmptyException {
            Message msg = new Message(issuerId, issuerId, dummyText);
            msg.setSenderId(issuerId);

            return msg;
        }

        @Override
        public Message pop(int issuerId) throws QueueEmptyException {
            Message msg = new Message(issuerId, issuerId, dummyText);
            msg.setSenderId(issuerId);

            return msg;
        }

        @Override
        public void pushMessage(Message msg) {

        }
    }

	@Override
	public List<Integer> retrieveQueueIndicesWithMessages(int issuerId) {
		List<Integer> result = new ArrayList<Integer>();
		
		try {
			CallableStatement statement = getPostgreSQLConnection().prepareCall(" { call pollQueues(?) } ");
			
			// Set input
			statement.setInt(1, issuerId);
			
			ResultSet resultSet = statement.executeQuery();
			
			while (resultSet.next()) {
				int queueId = resultSet.getInt(1);
				
				// Add to bucket
				result.add(queueId);
			}
			
		} catch (SQLException e) {
			logger.error(PostgreSQLStorageManagement.marker, "Fatal SQL error: Couldn't poll for messages!");
		}
		
		return result;
	}

	/**
	 * Abstraction of a queue built with PostgreSQL
	 * @author lennart
	 *
	 */
	public class PostgreSQLQueue extends Queue {

		protected PostgreSQLQueue(int id) {
			super(id);
		}

		// TODO: sourceId unnecessary?
		@Override
		public Message peek(int issuerId) throws QueueEmptyException {
			try {
				CallableStatement statement = PostgreSQLStorageManagement.this.postgreSQLConnection.prepareCall(" { call peekQueue(?, ?) } ");
				
				// Set input
				statement.setInt(1, this.getId());
				statement.setInt(2, issuerId);

				long startTime = System.nanoTime();
				ResultSet resultSet = statement.executeQuery();
				long duration = TimeUnit.NANOSECONDS.toMicros(System.nanoTime() - startTime);
                logger.info(dbTimeMarker, Long.toString(duration));

				if (!resultSet.next())
					throw new QueueEmptyException();
				
				// Get the topmost message
				int senderId = resultSet.getInt(1);
				int msgId = resultSet.getInt(2);
				String text = resultSet.getString(3);
				
				Message msg = new Message(this.getId(), issuerId, text);
				msg.setSenderId(senderId);
				msg.setMsgId(msgId);
				
				return msg;
				
			} catch (SQLException e) {
				logger.error(PostgreSQLStorageManagement.marker, "Fatal SQL error: Couldn't peek message from queue!");
			}
			
			return null;
		}

		@Override
		public Message pop(int issuerId) throws QueueEmptyException {
			try {
				CallableStatement statement = PostgreSQLStorageManagement.this.postgreSQLConnection.prepareCall(" { call popQueue(?, ?) } ");
				
				// Set input
				statement.setInt(1, this.getId());
				statement.setInt(2, issuerId);

				long startTime = System.nanoTime();
				ResultSet resultSet = statement.executeQuery();
				long duration = TimeUnit.NANOSECONDS.toMicros(System.nanoTime() - startTime);
                logger.info(dbTimeMarker, Long.toString(duration));

				if (!resultSet.next())
					throw new QueueEmptyException();
				
				// Get the topmost message
				int senderId = resultSet.getInt(1);
				int msgId = resultSet.getInt(2);
				String text = resultSet.getString(3);
				
				Message msg = new Message(this.getId(), issuerId, text);
				msg.setSenderId(senderId);
				msg.setMsgId(msgId);
				
				return msg;
				
			} catch (SQLException e) {
				logger.error(PostgreSQLStorageManagement.marker, "Fatal SQL error: Couldn't pop message from queue! " + e.getMessage());
			}
			
			return null;
		}

		@Override
		public void pushMessage(Message msg) {
			try {
				CallableStatement statement = PostgreSQLStorageManagement.this.postgreSQLConnection.prepareCall(" { call addMessage(?, ?, ?, ?) } ");
				
				// Set input
				statement.setInt(1, this.getId());
				statement.setInt(2,  msg.getSenderId());
				statement.setInt(3, msg.getReceiverId());
				statement.setString(4, msg.getMsg());
				
				long startTime = System.nanoTime();
				ResultSet resultSet = statement.executeQuery();
				long duration = TimeUnit.NANOSECONDS.toMicros(System.nanoTime() - startTime);
                logger.info(dbTimeMarker, Long.toString(duration));

				resultSet.next();
				
				int messageId = resultSet.getInt(1);
				
				// Assign message id
				msg.setMsgId(messageId);
				
				
			} catch (SQLException e) {
				logger.error(PostgreSQLStorageManagement.marker, "Fatal SQL error: Couldn't add message to queue!");
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public Account addAccount(String issuerName) {
		try {
			// Check if account already exists
			CallableStatement hasStatement = getPostgreSQLConnection().prepareCall(" { call hasAccount(?) } ");
			
			hasStatement.setString(1, issuerName);
			
			ResultSet hasAccountSet = hasStatement.executeQuery();
			hasAccountSet.next();
			
			int hasAccount = hasAccountSet.getInt(1);
			
			if (hasAccount != -1)
				return new Account(issuerName, hasAccount);
			
			CallableStatement statement = getPostgreSQLConnection().prepareCall(" { call createAccount(?) } ");
			
			statement.setString(1, issuerName);
			ResultSet resultSet = statement.executeQuery();
			resultSet.next();
			
			// Get account object
			int accountId = resultSet.getInt(1);
			Account account = new Account(issuerName, accountId);
			
			return account;
		} catch (SQLException e) {
			logger.error(PostgreSQLStorageManagement.marker, "Fatal SQL error: Couldn't create account!");
		}
		
		return null;
	}

	@Override
	public Message peekMessageFromSender(int issuerId, int sourceId) throws QueueEmptyException {
		try {
			// Prepare statement
			CallableStatement pollStatement = getPostgreSQLConnection().prepareCall(" { call pollQueuesForSender(?, ?) } ");
			
			// Set input
			pollStatement.setInt(1, issuerId);
			pollStatement.setInt(2, sourceId);
			
			ResultSet resultSet = pollStatement.executeQuery();
			
			if (!resultSet.next())
				throw new QueueEmptyException();
			
			int mId = resultSet.getInt(1);
			int qId = resultSet.getInt(2);
			String text = resultSet.getString(3);
			
			Message msg = new Message(qId, issuerId, text);
			msg.setSenderId(sourceId);
			msg.setMsgId(mId);
			
			return msg;
			
		} catch (SQLException e) {
			logger.error(PostgreSQLStorageManagement.marker, "Fatal SQL error: Couldn't retrieve messages from particular sender!");
		}
		return null;
	}

	@Override
	public Message popMessageFromSender(int issuerId, int sourceId)  throws QueueEmptyException {
		Message peeked = peekMessageFromSender(issuerId, sourceId);
		
		if (peeked != null)
		{
			int mId = peeked.getMsgId();
			// Remove message
			try {
				CallableStatement statement = PostgreSQLStorageManagement.this.postgreSQLConnection.prepareCall(" { call deleteMessage(?) } ");
				
				statement.setInt(1, mId);
				statement.execute();
				
				return peeked;
			} catch (SQLException e) {
				logger.error(PostgreSQLStorageManagement.marker, "Fatal SQL error: Couldn't pop(delete) message!");
			}
		}
		
		return null;
	}
}
