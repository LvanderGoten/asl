package Utility;

import java.io.Serializable;

public class Account implements Serializable {

	private static final long serialVersionUID = 1701700417577122872L;
	private String name;

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Account)
		{
			Account b = (Account) obj;
			
			return this.accountId == b.accountId;
		}
		
		return false;
	}

	@Override
	public String toString() {
		return this.getName();
	}

	private int accountId;
	
	/**
	 * @param name Textual representation of the nickname of the user
	 * @param accountId Unique identifier of the user
	 */
	public Account (String name, int accountId)
	{
		this.name = name;
		this.accountId = accountId;
	}

	public String getName() {
		return name;
	}

	public int getAccountId() {
		return accountId;
	}
}