package Exception;

/**
 * Indicates that a queue is either empty or there are no messages for a particular user
 * @author lennart
 *
 */
public class QueueEmptyException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6431652836980842149L;

}
